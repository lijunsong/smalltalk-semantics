(* Initital Environment and Store *)
(* 2 steps to produce a working environment *)

open Core_value
open Prims
open Printf
open Core_util
open Parse
open Desugar
open Eval

(* 1. Generate empty classes and metaclasses, and nil.
 * empty means all information is nil *)

(* classes *)
let classes = [
  "Object";
  "UndefinedObject";
  "Behavior";
  "ClassDescription";
  "Class";
  "Metaclass";
  (** Libraries **)
  "String";
  "ByteString";

  "Magnitude";
  "Number";
  "Integer";
  "SmallInteger";

  "BlockClosure";
  
  "Collection";
  "Array";
]

let mt_class (name : string) =
  VClass ("nil", [], mt_methods, name, mt_subs)

(* allocation two slots in store, one for value, one for the reference of
 * the value. *)
let alloc_with_ref (v : value) store : loc * store =
  let new_loc, store' = alloc v store in
  alloc (VObjLoc new_loc) store'

let rec bind_ref_list (x : string list) (v : value list) genv store : genv * store =
  match x, v with
  | [], [] -> genv, store
  | _, []
  | [], _ -> failwith "builtins: bind_reference_list"
  | x :: tlx, v :: tlv ->
    let _, genv', store' = alloc_and_bind_ref x v genv store in
    bind_ref_list tlx tlv genv' store'

(* step 1 bind classes and metaclass to the reference of VClass and 
   nil/true/false to the reference of VObjects *)
let create_classes (clses : string list) : genv * store =
  let genv, store = bind_ref_list ["nil"; "true"; "false"]
      [VObject ("UndefinedObject", VNil, mt_env);
       VObject ("True", VNil, mt_env);
       VObject ("False", VNil, mt_env)] mt_env mt_store in
  let vs = List.map (fun name ->  mt_class name) clses in
  let metas = List.map (fun name -> get_classname name) clses in
  let metavs = List.map (fun meta -> mt_class meta) metas in
  let gen, sto = bind_ref_list clses vs genv store in
  bind_ref_list metas metavs gen sto

(* 2. Add super class information *)
let super_mapping = [
  ("Behavior", "Object");
  ("UndefinedObject", "Object");
  ("ClassDescription", "Behavior");
  ("Class", "ClassDescription");
  ("Metaclass", "ClassDescription");
  (get_classname "Object", "Class");
  
  ("String", "Object");
  ("ByteString", "String");
  
  ("Magnitude", "Object");
  ("Number", "Magnitude");
  ("Integer", "Number");
  ("SmallInteger", "Integer");

  ("BlockClosure", "Object");

  ("Collection", "Object");
  ("Array", "Collection");
]

(* update the class' super and _meta's super_
 * NOTE: update the _original_ objects, not the referenced one. *)
let rec update_super (superinfo : (string * string) list) genv store : genv * store =
  let update base super genv store : genv * store =
    match fetch ~realobj:false base genv store with
    | Some (VObjLoc baseloc) -> begin match real_object (VObjLoc baseloc) store with
        | VClass (_, vars, dict, name, subs) ->
          let basev = VClass (super, vars, dict, name, subs) in
          let super_loc, superv = match fetch ~realobj:false super genv store with
            | Some (VObjLoc superloc as sl) -> begin match real_object sl store with
                | VClass (f1, f2, f3, f4, subclasses) ->
                  superloc, VClass (f1, f2, f3, f4, base :: subclasses)
                | _ -> failwith "super is not a VClass"
              end
            | _ -> failwith "update super, but super is invalid"
          in
          let store' = update_store baseloc basev store in
          genv, update_store super_loc superv store'
        | _ -> failwith "update_super: the base class is not a VClass"
      end
    | _ -> failwith
             (sprintf "update_super: base %s is not an VObjLoc/does not exist" base)
  in
  match superinfo with
  | [] -> genv, store
  | (base, super) :: tl ->
    let genv', store' = update base super genv store in
    (* prevent update (get_classname ^object), which is Metaclass, to something *)
    let gen, sto = if not (is_metaclass base) then
        update (get_classname base) (get_classname super) genv' store'
      else genv', store'
    in
    update_super tl gen sto

(* 3. Now the environment is working. *)
let setup_classes () : genv * store =
  let g, s = create_classes classes in
  update_super super_mapping g s

let builtins_path = "./kernel/builtins/"
let lib_path = "./kernel/libs/"
  
(* 4. set up methods and other libraries.
   When bootstrap starts, methods must be set up prior to libs *)
let method_files = list_directory ~list_only_files:true builtins_path
let files = list_directory ~list_only_files:true lib_path

(* Given genv and store that contains classes, setup methods in the genv and store *)
let rec setup_methods filepaths genv store : genv * store =
  match filepaths with
  | [] -> genv, store
  | filepath :: tl ->
    try
      let contents = string_of_file filepath in
      let stexp = match suffix_core filepath, suffix_st filepath with
        | false, false -> failwith ("found Unknown file: " ^ filepath)
        | true, _ -> parse_stexp_source contents
        | _, true -> parse_smalltalk_string contents
      in
      let exp = desugar stexp in
      let _, genv', store' = eval exp mt_env genv store in
      setup_methods tl genv' store'
    with err -> begin
        printf "error occurs on file %s:\n" filepath;
        print_endline (Printexc.to_string err);
        exit (-1)
      end

let bootstrap () : genv * store =
  let g, s = setup_classes () in
  let all_file = method_files @ files in
    setup_methods all_file g s
