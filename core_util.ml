
let verbose = ref false
let dprint (msg : string) =
  if !verbose then
    Printf.printf "%s%!" msg
      
let third (_,_,c) = c

(* Read a file *)
let string_of_file filepath =
  let inchan = open_in filepath in
  let buf = String.create (in_channel_length inchan) in
  really_input inchan buf 0 (in_channel_length inchan);
  buf

let rec read_all channel lines : string =
  try
    let line = input_line channel in
    read_all channel (line :: lines)
  with End_of_file -> String.concat "\n" (List.rev lines)

let rec read_all_stdin () : string =
  read_all stdin []

let split str delimiter : string list =
  Str.split (Str.regexp delimiter) str

let split_onspace str : string list =
  split str " +"

let has_substring str sub : bool =
  try
    let _ = Str.search_forward (Str.regexp sub) str 0 in
    true
  with _ -> false    

(* return the *path* of files in the directory *)
let list_directory ?(list_only_files =true) dir : string list =
  try
    let all = Sys.readdir dir in
    let names = List.map (fun name ->
        dir ^ "/" ^ name) (Array.to_list all) in
    if not list_only_files then names
    else
      List.filter (fun name ->
          not (Sys.is_directory name)) names
  with Failure msg -> failwith ("list_directory " ^ dir ^ ": "^ msg)


let has_suffix filepath (suffix_regexp : string) : bool =
  try
    let _ = Str.search_forward (Str.regexp suffix_regexp) filepath 0 in
    true
  with _ -> false

let suffix_st filepath : bool =
  has_suffix filepath "\\.st$"

let suffix_core filepath : bool =
  has_suffix filepath "\\.core$"

let double_quote_to_single str : string =
  Str.global_replace (Str.regexp "''") "'" str
