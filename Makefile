.PHONY: all clean test main testst

TEST_SRC = $(wildcard test/*.ml)
TEST_ST = $(wildcard test/*.st)
TEST_BYTE = $(subst .ml,.d.byte,$(TEST_SRC))
BUILD = ocamlbuild -Is sexp,test -pkgs oUnit,unix,str

all: $(TEST_BYTE) main

main:
	 $(BUILD) main.d.byte

clean:
	rm -rf *.d.byte
	rm -rf _build/

$(TEST_BYTE): %.d.byte: %.ml
	$(BUILD) $@

test: all $(TEST_BYTE) testst
	for p in $(TEST_BYTE); do \
		echo "Running $$p"; \
		_build/$$p -no-output-file -no-cache-filename; \
	done

testst:
	for p in $(TEST_ST); do \
		echo "Running $$p"; \
	    ./main.d.byte -load-st $$p -answer; \
		[ $$? -eq 0 ] && echo "pass" || echo "fail"; \
	done
