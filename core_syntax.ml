open Printf

type id = string

type cexp =
  | Id of string
  | Num of int
  | String of string
  | Array of cexp list
  | Let of string * cexp * cexp
  | Seq of cexp * cexp
  | Assign of string * cexp
  | WithLabel of label * cexp
  | Return of label * cexp
  | Lam of string list * cexp
  | App of cexp * cexp list
  | Prim of string * cexp list
  (* Lookup: methodname, lookup in which class, self, arguments *)
  | Lookup of string * cexp * cexp * cexp list
and label = int

let rec string_of_cexp = function
  | Id x -> sprintf "(Id %s)" x
  | Num n -> sprintf "(Num %d)" n
  | String s -> sprintf "(String \"%s\")" s
  | Array elms ->
    sprintf "(Array (%s))"
      (String.concat " " (List.map string_of_cexp elms))
  | Let (x, e, body) ->
    sprintf "(Let %s %s %s)" x (string_of_cexp e)
      (string_of_cexp body)
  | Seq (e1, e2) ->
    sprintf "(Seq %s %s)" (string_of_cexp e1) (string_of_cexp e2)
  | Assign (x, e) ->
    sprintf "(Assign %s %s)" x (string_of_cexp e)
  | WithLabel (n, e) ->
    sprintf "(WithLabel %d %s)" n (string_of_cexp e)
  | Return (n, e) ->
    sprintf "(Return %d %s)" n (string_of_cexp e)
  | Lam (params, body) ->
    sprintf "(Lam (%s) %s)" (String.concat " " params)
      (string_of_cexp body)
  | App (e, es) ->
    sprintf "(App %s (%s))" (string_of_cexp e)
      (String.concat " " (List.map string_of_cexp es))
  | Prim (op, es) ->
    sprintf "(Prim \"%s\" (%s))" op
      (String.concat " " (List.map string_of_cexp es))
  | Lookup (name, cls, exp, exps) ->
    sprintf "(Lookup %s %s %s (%s))"
      name (string_of_cexp cls) (string_of_cexp exp) (String.concat " " (List.map string_of_cexp exps))

let _self_ = "*self*"
let _super_ = "*super*"
let _super_lit_ = "super" (* super as it is. *)
let _nil_ = "nil"
let _true_ = "true"
let _false_ = "false"
let _notunderstand_ = "doesNotUnderstand"

module IdSet = Set.Make (String)
