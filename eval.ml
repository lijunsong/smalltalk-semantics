open Core_syntax
open St_syntax
open Core_value
open Printf
open Prims
open Desugar
open Parse
open Core_util

let eval_error str =
  raise (EvalError str)
    
let rec eval (exp : cexp) (env : env) (genv : genv) (store : store) : result =
  match exp with
  | Num _
  | String _
  | Array _
  | Lam (_, _) ->
    eval_primv exp env genv store
  | Id x ->
    (* first look in local env, then in global if not found in local *)
    begin match fetch ~realobj:false x env store with
      | Some (v) -> v, genv, store
      | None ->
        begin match fetch ~realobj:false x genv store with
          | Some (v) -> v, genv, store
          | None when x <> _nil_ ->
            eval (Id _nil_) env genv store
          | None -> eval_error "nil is unbound in global environment"
        end
    end
  | Seq (e1, e2) ->
    let _, genv', store' = eval e1 env genv store in
    eval e2 env genv' store'
  | Let (x, xe, body) ->
    let xv, genv', store' = eval xe env genv store in
    let env', sto = add_bind x xv env store' in
    eval body env' genv' sto
  | Assign (x, e) ->
    (* if x is not bound anywhere, bind it in current env, otherwise, update it *)
    let ev, genv', store' = eval e env genv store in
    let g, s = match is_bound x env, is_bound x genv' with
      | false, false -> error_eval (x ^ " is unbound")
      | true, _ ->
        dprint (sprintf "assign: %s. env is\n%s\n"
                          (string_of_cexp exp) (string_of_env env));
        let _ = match lookup_env x env with
          | None -> failwith "nonreachable"
          | Some (loc) ->
            dprint (sprintf "update bind %s -> %d -> %s\n" x loc
                      (string_of_value ev)) in
        let _, sto = update_bind x ev env store' in
        genv', sto
      | _, true ->
        update_bind x ev genv' store'
    in
    ev, g, s
  | App (f, args) -> (* seems unused. *)
    let fv, genv', store' = eval f env genv store in
    let argsv, gen, sto = eval_exps args env genv' store' in
    eval_app (real_object fv sto) argsv gen sto
  | WithLabel (l, e) ->
    begin try
        eval e env genv store
      with
      | VReturn (l', v, genv', sto) when l = l' -> v, genv', sto
      | VReturn (_, v, _, _) ->
        eval_error "Return out of context"
    end
  | Return (l, e) ->
    let v, genv', store' = eval e env genv store in
    raise (VReturn (l, v, genv', store'))
  | Prim (op, args) ->
    let vs, genv', store' = eval_exps args env genv store in
    eval_prim op vs genv' store'
  | Lookup (mthod_name, cls, self, args) ->
    let clsv, gen', sto' = eval cls env genv store in
    let recv, genv', store' = eval self env gen' sto' in
    let argvs, gen, sto = eval_exps args env genv' store' in
    match lookup_method clsv mthod_name recv gen sto with
    | None when mthod_name = _notunderstand_ ->
      error_eval "Error: doesNotUnderstand is not found. Abort."
    | None ->
      let _ = if !verbose then
          printf "%s is not found. send doesNotUnderstand.\n" mthod_name in
      eval (Lookup (_notunderstand_, cls, self, [String mthod_name]))
        env gen sto
    | Some (methodv) ->
      eval_closure methodv (recv :: argvs) gen sto

and eval_primv (exp : cexp) env genv store : result =
  let primv = match exp with
    | Num n -> VNum n
    | String s -> VString s
    | Lam (params, body) -> VClosure (params, body, env)
    | Array elms -> eval_error "not implemented. Not used"
    | _ -> eval_error (errstr "primv" (string_of_cexp exp)) in
  primv_to_objv primv genv store
  
(* Evaluate exp list with the same environment.
 * Pass the store for change *)
and eval_exps (exp : cexp list) (env : env) (genv : genv) (store : store)
  : value list * genv * store =
  match exp with
  | [] -> [], genv, store
  | hd :: tl ->
    let v, genv', store' = eval hd env genv store in
    let vs, gen, stor = eval_exps tl env genv' store' in
    v :: vs, gen, stor

and eval_closure (fv : primv) (argsv : value list) (genv : genv) (store : store) : result =
  match fv with
  | VClosure (params, body, env) ->
    if (List.length params) <> (List.length argsv) then begin
      error_eval (sprintf "arity mismatch: expect %d, but got %d\nExpect: %s\nGot:%s\n"
                    (List.length params) (List.length argsv)
                    (String.concat ";" params)
                    (String.concat ";" (List.map string_of_value argsv)))
    end else
      let _ = dprint (sprintf "bind params [%s] ~ [%s]\n"
                        (String.concat "," params)
                        (string_of_value_list argsv)) in
      (* argsv shall all be VObjLoc, so just bind them without creating 
         new ref location *)
      let env', store' = add_bind_list params argsv env store in
      let _ = dprint (sprintf "2. eval body: %s, given the environment: \n%s\n===\n"
                        (string_of_cexp body) (string_of_env env')) in
        eval body env' genv store'
  | _ -> error_eval (sprintf "eval_closure found non-closure: %s" (string_of_primv fv))
           
and eval_app (fv : value) (argsv : value list) (genv : genv) (store : store)
  : value * genv * store =
  match fv with
  | VObjLoc loc -> unexpected_VObjLoc "eval_app" fv
  | VObject (_, closurev, _) ->
      eval_closure closurev argsv genv store
  | _ ->
    error_internal
      (sprintf "eval_app: found non closure:%s" (string_of_value fv))
    
and eval_prim (op : string) (vs : value list) (genv : genv) (store : store)
  : value * genv * store =
  match op with
  | "compile" -> begin match vs with
      | [cls; code] ->
        eval_compile (real_object cls store) (real_object code store) genv store
      | _ -> failwith (sprintf "eval_compile requires 2 arguments, but got %d"
                         (List.length vs))
    end
  | _ -> (lookup_prim op store) vs genv store

(* compile smalltalk code for cls *)
and eval_compile (cls : value) (code : value) genv store : result =
  match cls with
  | VObject (_, _, _) -> error_eval (err_value_msg "VClass (in eval_compile)" cls)
  | VClass (_, _, _, name, _) ->
    let code_str = raw_string code in
    let code_str' = double_quote_to_single code_str in
    let stexp = update_method_class (parse_smalltalk_method code_str') name in
    let exp = desugar stexp in
    let _ = dprint (sprintf "compile %s to exp %s" code_str' (string_of_cexp exp)) in
    eval exp mt_env genv store
  | VObjLoc (_) -> unexpected_VObjLoc "eval_compile" cls


