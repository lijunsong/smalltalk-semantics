type sexp =
  | List of sexp list
  | Atom of string
  | Str of string

let rec string_of_sexp (e : sexp) : string =
  match e with
  | List es ->
    let str = String.concat " " (List.map string_of_sexp es) in
    Printf.sprintf "(%s)" str
  | Atom s -> s
  | Str s -> String.concat "" ["\""; s; "\""]

    
