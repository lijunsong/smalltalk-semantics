open Printf
open Sexp
    

(* produce sexp from s-expression string *)
let parse_sexp str : sexp list =
  let lexbuf = Lexing.from_string str in
  Sexp_parser.exp Sexp_lexer.token lexbuf
    
