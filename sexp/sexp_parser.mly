%{
open Sexp
%}
%token LP
%token RP
%token <string> ATOM
%token <string> STRING
%token EOF

%start exp
%type <Sexp.sexp list> exp

%%
/* A program includes multiple sexp */
exp:
  | EOF          {[]}
  | sexp exp {$1 :: $2}

;

sexp:
  | atom     {$1}
  | LP sexp_list RP { List $2 }
;

sexp_list:
  | /* empty */     { [] }
  | sexp sexp_list  { $1 :: $2 }
;

atom:
  | ATOM           { Atom $1 }
  | STRING         { Str $1 }
;
%%
