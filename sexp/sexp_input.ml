open Printf
    
(* file: main.ml *)
(* Assumes the parser file is "rtcalc.mly" and the lexer file is "lexer.mll". *)
let main () =
    let cin =
      if Array.length Sys.argv > 1
      then open_in Sys.argv.(1)
      else stdin
    in
    let lexbuf = Lexing.from_channel cin in
    let result =  Sexp_parser.exp Sexp_lexer.token lexbuf in
    List.map (fun e -> printf "result: %s" (Sexp.string_of_sexp e)) result
    
let _ = Printexc.print main ()
