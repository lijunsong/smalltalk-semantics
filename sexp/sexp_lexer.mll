{
  open Sexp_parser
}

rule token = parse
  | [' ' '\t' '\n'] { token lexbuf }
  | '(' { LP }
  | ')' { RP }
  | '"' { STRING (read_string "" lexbuf)  }
  | [^ '(' ')' ' ' '\t' '\n' '"']+
         { ATOM (Lexing.lexeme lexbuf) }
  | eof { EOF }

and read_string cur = parse
  | '"' { cur }
  | '\\' '"'
  | '\\' 'n'
  | '\\' 't'
  | '\\' '\''
  | [^ '"' '\\']+
    { read_string (String.concat "" [cur; Lexing.lexeme lexbuf]) lexbuf }

(*
{
  let main () =
    let cin =
      if Array.length Sys.argv > 1
      then open_in Sys.argv.(1)
      else stdin
    in
    let lexbuf = Lexing.from_channel cin in
    token lexbuf

  let _ =  main ()
}

*)
