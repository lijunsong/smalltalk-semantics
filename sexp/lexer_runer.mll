(* scanner for a toy language *)

{
  open Printf
}

let digit = ['0'-'9']
let id = ['a'-'z'] ['a'-'z' '0'-'9']*

rule token = parse
  | [' ' '\t'] { token lexbuf }
  | ['\n'] { printf "new line\n"; token lexbuf }
  | '(' { printf "LP\n"; token lexbuf }
  | ')' { printf "RP\n"; token lexbuf }
  | '"' { printf "string: "; 
          printf "%s\n" (read_string "" lexbuf);
          token lexbuf
        }
  | [^ '(' ')' ' ' '\t' '\n' '"']+
         { printf "atom: "; printf "%s\n" (Lexing.lexeme lexbuf);
          token lexbuf }
  | eof { printf "done\n" }

and read_string cur = parse
  | '"' { cur }
  | '\\' '"'
  | '\\' 'n'
  | '\\' 't'
  | '\\' '\''
  | [^ '"' '\\']+
    { read_string (String.concat "" [cur; Lexing.lexeme lexbuf]) lexbuf }

{
  let main () =
    let cin =
      if Array.length Sys.argv > 1
      then open_in Sys.argv.(1)
      else stdin
    in
    let lexbuf = Lexing.from_channel cin in
    token lexbuf

  let _ =  main ()
}