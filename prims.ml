open Core_syntax
open Core_value
open Core_util
open Printf

(* 
   with_*arg_obj: fetch the real object (rather than the reference location);
   with_*arg_loc: pass the location to the function.
 *)
let with_1arg_obj ?(f="") lam store = function
  | [v] -> lam (real_object ~f v store)
  | _ -> error_prim "with_1arg_obj: arity mismatch"

let with_2args_obj ?(f="") lam store = function
  | [v1; v2] -> lam (real_object ~f v1 store) (real_object ~f v2 store)
  | _ -> error_prim "with_2args_obj: arity mismatch"

let with_3args_obj ?(f="") lam store = function
  | [v1; v2; v3] ->
    lam (real_object ~f v1 store) (real_object ~f v2 store) (real_object ~f v3 store)
  | _ -> error_prim "with_3args_obj: arity mismatch"

let with_2args_loc lam store = function
  | [v1; v2] -> lam v1 v2
  | _ -> error_prim "with_2args_loc: arity mismatch"

let with_VClass (store : store) v lam =
  match (real_object ~f:"with_VClass" v store) with
  | VClass (f1, f2, f3, f4, f5) -> lam (f1, f2, f3, f4, f5)
  | VObject (_, _, _) ->
    error_prim (err_value_msg "VClass (in with_VClass)" v)
  | VObjLoc _ as v -> unexpected_VObjLoc "with_VClass" v
                        
let with_VObject (store : store) v lam =
  match (real_object v store) with
  | VClass (_, _, _, _, _) ->
    error_prim (err_value_msg "VClass (in with_VClass)" v)    
  | VObject (f1, f2, f3) -> lam (f1, f2, f3)
  | VObjLoc _ as v -> unexpected_VObjLoc "with_VObject" v
  
(* get the class name of the class whose name is passed in *)
let get_classname name =
  if String.get name 0 = '^' then
    "Metaclass"
  else
    "^" ^ name

(* Given a class name, fetch only the instance variables that are available
 * in the class *)
let get_instvar_names (clsname : string) genv store : string list =
  if clsname = _nil_ then []
  else match fetch ~realobj:true clsname genv store with
    | Some (VClass (super, vars, _, _, _)) -> vars
    | Some (v) -> error_prim ("get_instvar_names: " ^
                              (err_value_msg "VClass (in get_instvar_names)" v))
    | None -> error_prim (sprintf "get_instvar_names: class %s is not found"
                            clsname)

(* Given a class name, fetch all the instance variables that are available. *)
let rec get_all_instvar_names (clsname : string) genv store : string list =
  if clsname = _nil_ then []
  else match fetch ~realobj:true clsname genv store with
    | Some (VClass (super, vars, _, _, _)) ->
      vars @ get_all_instvar_names super genv store
    | Some (v) ->
      error_prim (sprintf "get_all_instvar_names: the class %s is not a VClass: %s"
                    clsname (string_of_value v))
    | None -> error_prim (sprintf "get_all_instvar_names: class %s is not found"
                            clsname)

(* The creation of an objv requires global env to get the location of nil *)
let objv (clsname : string) (primv : primv) genv store : value * store =
  let instvar_names = get_all_instvar_names clsname genv store in
  let env, sto = init_nil_bindings instvar_names genv store in
  let v = VObject (clsname, primv, env) in
  let loc, store' = alloc v sto in
  VObjLoc loc, store'

(* use this to allocate new object/class *)
let primv_to_objv (primv : primv) genv store : result =
  let v, sto = match primv with
    | VNum n -> objv "SmallInteger" primv genv store
    | VString s -> objv "ByteString" primv genv store
    | VArray _ -> objv "Array" primv genv store 
    | VClosure _ -> objv "BlockClosure" primv genv store
    | VNil -> get_nilv genv store, store
  in
  v, genv, sto

(* get the class of the class whose name is passed in *)
let get_classofclass (whichcls : string) genv store : value =
  match fetch ~realobj:false (get_classname whichcls) genv store with
  | None -> error_prim (sprintf "%s's class is not found" whichcls)
  | Some (v) -> v

(* get the class of the given VClass/VObject *)
let get_class (v : value) genv store : value =
  let objv = real_object ~f:"get_class" v store in
  match objv with
  | VObject (cls, _, _) -> begin match fetch ~realobj:false cls genv store with
      | None -> error_prim (sprintf "the class(%s) of %s is not found"
                              cls (string_of_value objv))
      | Some (v) -> v
    end
  | VClass (_, _, _, name, _) -> get_classofclass name genv store
  | VObjLoc loc -> unexpected_VObjLoc "op_class" objv

let to_st_bool (b : bool) genv store : value =
  match b with
  | true -> fetch_or_fail ~realobj:false _true_ genv store
  | false -> fetch_or_fail ~realobj:false _false_ genv store


let rec same_object (v1 : value) (v2 : value) store : bool =
  dprint (sprintf "same_object: comparing %s ~~ %s\n" (string_of_value v1) (string_of_value v2));
  match v1, v2 with
  | VObjLoc l1, VObjLoc l2 ->
    l1 = l2
  (* any other two are not equal *)
  | _ -> false

let same_object_by_name (id1 : id) (id2 : id) genv store : bool =
  match fetch ~realobj:false id1 genv store,
        fetch ~realobj:false id2 genv store with
  | Some (v1), Some (v2) -> same_object v1 v2 store
  | None, _ -> error_prim (sprintf "same_object_by_string gets unbound id: %s" id1)
  | _, None -> error_prim (sprintf "same_object_by_string gets unbound id: %s" id2)

let same_object2 (id1 : id) (v2 : value) genv store : bool =
  match fetch ~realobj:false id1 genv store with
  | Some (v1) -> same_object v1 v2 store
  | None -> error_prim (sprintf "same_object2 gets unbound id: %s" id1)

(* Create a new instance: VObject or VClass *)
let op_new_vobject (clsv : value) genv store : result =
  with_VClass store clsv (fun (super, vars, dict, name, _) ->
      if is_metaclass name || name = "Metaclass" then
        error_prim ("cannot create a new VObject from a metaclass: " ^ name)
      else
        let v, store' = objv name VNil genv store in
        v, genv, store')

let op_new_vclass (clsv : value) genv store : result =
  with_VClass store clsv (fun (super, vars, dict, name, _) ->
      if not (is_metaclass name) && name <> "Metaclass" then
        error_prim ("cannot create a new VClass from a normal class: " ^ name)
      else
        (* directly return a meta class, because this is only called by Metaclass*)
        let v = VClass ("Object", [], mt_methods, "^anonymous", []) in
        let loc, store' = alloc v store in
        VObjLoc loc, genv, store'
    )

let op_class (clsv : value) genv store : result =
  match clsv with
  | VObjLoc loc -> unexpected_VObjLoc "op_class" clsv
  | _ -> get_class clsv genv store, genv, store

let op_class_name (clsv : value) genv store : result =
  match clsv with
  | VObject (cls, _, _) -> primv_to_objv (VString cls) genv store
  | VClass (_, _, _, name, _) ->
    primv_to_objv (VString (get_classname name)) genv store
  | VObjLoc _ -> unexpected_VObjLoc "op_class_name" clsv

let op_superclass (clsv : value) genv store : result =
  with_VClass store clsv (fun (super,_,_,name,_) ->
      match fetch ~realobj:false super genv store with
      | None -> error_prim (sprintf "super class %s of %s is unbound" super name)
      | Some (v) -> v, genv, store
    )

let op_same_object (v1 : value) (v2 : value) genv store : result =
  dprint (sprintf "op_same_objec: comparing %s ~~ %s\n" (string_of_value v1) (string_of_value v2));  
  to_st_bool (same_object v1 v2 store) genv store, genv, store

(* get instance variable names of the passed in class *)
let op_instvar_names (clsv : value) genv store : result =
  (*let names : string list =  with_VClass store clsv (fun (_, instvars, _, _, _) -> instvars) in
  let objvs : value list =
    List.map (fun sto name ->
        let v, g, s = primv_to_objv (VString name) genv sto in
        
      ) names in
    primv_to_objv (VArray objvs) genv store*)
  failwith "Not Implemented"

let add_metaclass (metaname : string) (super_meta_name : string) genv store : result =
  let clsv = VClass (super_meta_name, [], mt_methods, metaname, mt_subs) in
  alloc_and_bind_ref metaname clsv genv store

(* Create a new subclass(and its metaclass) that inherited from baseclass.
 * New global env and new store that contains the new class are also returned. 
 * subclassname is a string
 * subclass_instvars is a string of var names that is separated by string
   /or VArray of string of vars
*)
let op_subclass (baseclass : value) (subclassname : value) (subclass_instvars : value) genv store : result =
  let subclass_name = raw_string subclassname in
  match baseclass with
  | VObjLoc _ -> unexpected_VObjLoc "op_subclass" baseclass
  | VObject (_, _, _) -> error_prim (err_value_msg "VClass (in op_subclass)" baseclass)
  | VClass (super, vars, dict, basename, subclasses) ->
    (* 1. create metaclass first if the metaclass does not exist ! *)
    let metaname = get_classname subclass_name in
    assert (not (is_bound metaname genv));
    let super_meta_name = get_classname basename in
    let _, genv', store' = add_metaclass metaname super_meta_name genv store in
    (* 2. get the names of instance variables that are defined for this subclass *)
    let instvar_names = match subclass_instvars with
      | VObject (_, VString s, _) -> split_onspace s
      | VObject (_, VArray s, _) -> map_VArray raw_string subclass_instvars 
      | _ -> error_prim "op_subclass: instanceVariableNames must be a string or Array"
    in
    let subv = VClass (basename, instvar_names, mt_methods, subclass_name, mt_subs) in
    (* 3. update base class's subclass fields *)
    let new_basev = VClass (super, vars, dict, basename, subclass_name :: subclasses) in
    (* _update_ base class *)
    let gen, sto = update_bind_origin basename new_basev genv' store' in
    (* allocate space for subclass *)
    let subref, gen', sto' = alloc_and_bind_ref subclass_name subv gen sto in
    subref, gen', sto'

let op_set_superclass (clsv : value) (super_name : value) genv store : result =
  let superclass_name = raw_string super_name in
  with_VClass store clsv (fun (_, vars, dict, name, subclasses) ->
      let new_class = VClass (superclass_name, vars, dict, name, subclasses) in
      let gen, sto = update_bind_origin name new_class genv store in
      let refv = fetch_or_fail ~realobj:false name gen sto in
      refv, gen, sto
    )      

(* cls: is class in which the method is looked up.
 * sel: is the method name
 * self: is the receiver initiating the method lookup, used for setting up 
   environment for the method if self is an Object
*)
let lookup_method (cls : value) (method_name : string) (self : value) genv store : primv option =
  with_VClass store (real_object ~f:"lookup_method cls" cls store) (fun (super,vars,dict,name,subclasses) ->
      match lookup_methodv_follow_chain method_name name genv store  with
      | None ->
        (* if the method is not found in the whole class chains,
         * the method is not understood. 
         * Then, send message "doesNotUnderstand" with arg "sel" to self *)
        None
      | Some (Lam (params, body)) ->
        let env' = match real_object ~f:"lookup_method self" self store with
          (*if self is an object, it must has its own env for instance variables *)
          | VObject (_, _, env) -> env
            (* if self is a class, all instance variables is in its structures,
             * it does not matter for the env. *)
          | VClass _ -> mt_env
          | VObjLoc _ -> unexpected_VObjLoc "lookup_method" self
        in
        Some (VClosure (params, body, env'))
      | Some (_) ->
        error_prim "lookup_method: find non-Lam in method dict!"
    )
  

let rec op_new_method (clsname : value) (pat : value) (closure : value) genv store : result =
  let classname = raw_string clsname in
  let pattern = raw_string pat in
  match fetch ~realobj:true classname genv store with
  | None ->
    error_prim ("op_new_method: " ^ classname ^ "not found")
  | Some (VClass (super, vars, dict, name, subs)) ->
    (* OK, class name is pointing to a class. *)
    begin match closure with
      | VObject (_, VClosure (params, body, _), _) ->
        (* OK, closure is pointing to a closure *)
        let lam : cexp = Lam (params, body) in
        let new_dict = update_methodv pattern lam dict in
        let new_class = VClass (super, vars, new_dict, name, subs) in
        let genv', store' = update_bind_origin name new_class genv store in
        get_nilv genv' store', genv', store'
      | _ -> error_prim (err_value_msg "VClosure (in op_new_method)" closure)
    end
  | Some (obj) ->  error_prim (err_value_msg "VClass" obj)

let op_print (str : value) genv store : result =
  let s = raw_string str in
  printf "%s%!" (Scanf.unescaped s); (* unescape in case the string has \n *)
  get_nilv genv store, genv, store

let op_name (v : value) genv store : result =
  let str = match v with
    | VObject (_, _, _) -> (pretty_of_value v)
    | VClass (_, _, _, name, _) -> (pretty_of_name name)
    | VObjLoc _ -> unexpected_VObjLoc "op_name" v
  in
  primv_to_objv (VString str) genv store

let op_as_string (objv : value) genv store : result =
  let pretty = pretty_of_value objv in
  primv_to_objv (VString pretty) genv store

let op_raise (msg : value) genv store : result =
  let msg_str = match msg with
    | VObject (_, VString s, _) -> s
    | _ -> pretty_of_value msg
  in
  raise (VError msg_str)

let op_string_plus (s1 : value) (s2 : value) genv store : result =
  let str1 = raw_string s1 in
  let str2 = raw_string s2 in
  primv_to_objv (VString (str1 ^ str2)) genv store

let op_string_eq (s1 : value) (s2 : value) genv store : result =
  let eq = try
    let str1 = raw_string s1 in
    let str2 = raw_string s2 in
    str1 = str2
    with _ -> false
  in
  to_st_bool eq genv store, genv, store

let op_num_eq (n1 : value) (n2 : value) genv store : result =
  let e1 = try
      let num1 = raw_int n1 in
      let num2 = raw_int n2 in
      num1 = num2
    with _ -> false
  in
  to_st_bool e1 genv store, genv, store

let op_num_plus (n1 : value) (n2 : value) genv store : result =
  let e1 =
      let num1 = raw_int n1 in
      let num2 = raw_int n2 in
      num1 + num2
  in
  primv_to_objv (VNum e1) genv store

let op_string_to_num (s : value) genv store : result =
  let n = int_of_string (raw_string s) in
  primv_to_objv (VNum n) genv store
    
let lookup_prim op store =
  match op with
  | "class" -> with_1arg_obj op_class store
  | "classname" -> with_1arg_obj op_class_name store
  | "new-vobject" -> with_1arg_obj op_new_vobject store
  | "new-vclass" -> with_1arg_obj op_new_vclass store
  | "print" -> with_1arg_obj op_print store
  | "superclass" -> with_1arg_obj op_superclass store
                      
  | "name" -> with_1arg_obj op_name store
  | "as-string" -> with_1arg_obj op_as_string store
  | "string-to-num" -> with_1arg_obj op_string_to_num store
  | "raise" -> with_1arg_obj op_raise store
                 
  | "set-superclass" -> with_2args_obj op_set_superclass store
  | "string+" -> with_2args_obj op_string_plus store
  | "string=" -> with_2args_obj op_string_eq store
  | "same-object" -> with_2args_loc op_same_object store
  | "num=" -> with_2args_obj op_num_eq store
  | "num+" -> with_2args_obj op_num_plus store
                        
  | "new-method" -> with_3args_obj op_new_method store
  | "subclass" -> with_3args_obj op_subclass store

  | _ -> error_prim ("Unknown prim: " ^ op)
