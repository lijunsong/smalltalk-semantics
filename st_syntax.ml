open Printf
module S = Sexp
    
type stexp =
  | STId of string
  | STNum of int
  | STString of string
  | STNil
  | STSelf
  | STSuper
  | STTrue
  | STFalse
  | STSendUnary of stexp * string
  | STSendBinary of stexp * string * stexp
  | STSendKeyword of stexp * string * stexp list
  | STReturn of stexp
  | STSequence of string list * stexp list       (* temp * stmts *)
  | STAssign of string * stexp
  | STLam of string list * stexp (* body should only be STSequence *)
  | STApp of stexp * stexp list
  | STArray of stexp list
  | STPrim of string * stexp list (* op and args *)
  | STMethod of string * string * string list * stexp (* body should only be STSequence*)


let rec string_of_stexp (e : stexp) : string =
  let string_of_stexps (es : stexp list) : string =
    String.concat " " (List.map string_of_stexp es)
  in
  match e with
  | STId s ->
    sprintf "(STId %s)" s
  | STNum n ->
    sprintf "(STNum %d)" n
  | STString s ->
    sprintf "(STString \"%s\")" s
  | STNil -> "(STNil)"
  | STSelf -> "(STSelf)"
  | STSuper -> "(STSuper)"
  | STTrue -> "(STTrue)"
  | STFalse -> "(STFalse)"
  | STSendUnary (recv, sel) ->
    sprintf "(STSendUnary %s %s)" (string_of_stexp recv) sel
  | STSendBinary (recv, sel, arg) ->
    sprintf "(STSendBinary %s %s %s)"
      (string_of_stexp recv) sel (string_of_stexp arg)
  | STSendKeyword (recv, sel, args) ->
    (sprintf "(STSendKeyword %s %s (%s))"
       (string_of_stexp recv) sel (string_of_stexps args))
  | STReturn (e) ->
    (sprintf "(STReturn %s)" (string_of_stexp e))
  | STSequence (temps, stmts) ->
    (sprintf "(STSequence (%s) (%s))" (String.concat " " temps)
       (string_of_stexps stmts))
  | STAssign (x, e) -> sprintf "(STAssign %s %s)" x (string_of_stexp e)
  | STLam (xs, e) ->
    sprintf "(STLam (%s) %s)" (String.concat " " xs)
      (string_of_stexp e)
  | STArray (es) -> sprintf "(STArray (%s))" (string_of_stexps es)
  | _ -> sprintf "NYI"

(* convert s-expression into STExp *)
let rec stexp_of_sexp (e : S.sexp) : stexp =
  let fail () =
    failwith (sprintf "stexp_of_sexp: unknown %s" (S.string_of_sexp e))
  in
  let atom_list (lst : S.sexp list) : string list =
    List.map (fun t -> match t with
        | S.Atom x -> x
        | _ -> fail ())
      lst in
  match e with
  | S.Atom "STNil" -> STNil
  | S.Atom "STSelf" -> STSelf
  | S.Atom "STSuper" -> STSuper
  | S.Atom "STTrue" -> STTrue
  | S.Atom "STFalse" -> STFalse
  | S.List [S.Atom "STId"; S.Atom id] -> STId id
  | S.List [S.Atom "STNum"; S.Atom n] -> STNum (int_of_string n)
  | S.List [S.Atom "STString"; S.Str s] -> STString s
  | S.List [S.Atom "STNil"] -> STNil
  | S.List [S.Atom "STSelf"] -> STSelf
  | S.List [S.Atom "STSuper"] -> STSuper
  | S.List [S.Atom "STTrue"] -> STTrue
  | S.List [S.Atom "STFalse"] -> STFalse
  | S.List [S.Atom "STSendUnary"; receiver; S.Atom selector] ->
    STSendUnary (stexp_of_sexp receiver, selector)
  | S.List [S.Atom "STSendBinary"; receiver; S.Atom selector; arg] ->
    STSendBinary (stexp_of_sexp receiver, selector, stexp_of_sexp arg)
  | S.List [S.Atom "STSendKeyword"; receiver; S.Atom selector; S.List args] ->
    STSendKeyword (stexp_of_sexp receiver, selector,
                   List.map (fun arg -> stexp_of_sexp arg) args)
  | S.List [S.Atom "STReturn"; e] ->
    STReturn (stexp_of_sexp e)
  | S.List [S.Atom "STSequence"; S.List temps; S.List stmts] ->
    STSequence (atom_list temps,
                List.map (fun stmt -> stexp_of_sexp stmt) stmts)
  | S.List [S.Atom "STAssign"; S.Atom x; e] ->
    STAssign (x, stexp_of_sexp e)
  | S.List [S.Atom "STLam"; S.List xs; body] ->
    STLam (atom_list xs, stexp_of_sexp body)
  | S.List [S.Atom "STApp"; f; S.List args] ->
    STApp (stexp_of_sexp f, List.map stexp_of_sexp args)
  | S.List [S.Atom "STArray"; S.List elms] ->
    STArray (List.map (fun e -> stexp_of_sexp e) elms)
  | S.List [S.Atom "STMethod"; S.Atom clsname; S.Atom methodname;
            S.List args; body] ->
    STMethod (clsname, methodname, atom_list args,
              stexp_of_sexp body)
  | S.List [S.Atom "STPrim"; S.Atom op; S.List args] ->
    STPrim (op, List.map stexp_of_sexp args)
  | _ -> fail()

(* Given multiple STExp, chain them together by STSequence *)
let sequentialize_stexp (exps : stexp list) : stexp =
  STSequence ([], exps)

(* update the class field of STMethod *)
let rec update_method_class (exp : stexp) (cls : string) : stexp =
  let update_list (exps : stexp list) =
    List.map (fun e -> update_method_class e cls) exps
  in
  match exp with
  | STMethod (_, methodname, args, body) ->
    STMethod (cls, methodname, args, body)
  | STSequence (temps, stmts) ->
    STSequence (temps, update_list stmts)
  | _ -> exp
  
