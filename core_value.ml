open Core_syntax
open Printf
open Core_util

type loc = int
module Loc = struct
  type t = loc
  let compare = Pervasives.compare
end

module IdMap = Map.Make(String)
module LocMap = Map.Make(Loc)

type genv = loc IdMap.t (* global environment *)
type env = loc IdMap.t

type value =
  | VObjLoc of loc
  (* super, instance_var names, dict, name, subclasses*)  
  | VClass of string * id list * methodv  * string * id list
  (* class, value, env *)
  | VObject of string * primv * env
and primv =
  | VClosure of string list * cexp * env
  | VNum of int
  | VString of string
  | VArray of value list
  | VNil
and methodv = cexp IdMap.t (* method name -> Lam *)

let string_of_primv pv : string = match pv with
  | VNum n -> sprintf "VNum %d" n
  | VString s -> sprintf "VString %s" s
  | VArray arr -> sprintf "VArray"
  | VClosure (c,_,_) -> "a clsoure"
  | VNil -> "VNil"


let string_of_env (env : env) : string =
  let s = List.map (fun (k, l) -> sprintf "%30s -> %d" k l)
      (IdMap.bindings env) in
  String.concat "\n" s

(* for debugging, showing messages *)
let string_of_value v = match v with
  | VObjLoc loc -> "@" ^ (string_of_int loc)
  | VObject (cls, v, env) ->
    sprintf "(a %s)%s{%s}" cls (string_of_primv v) (string_of_env env)
  | VClass (super, vars, dict, name, subs) ->
    sprintf "Class %s(%s, [%s], methods [%s], subclasses [%s])" name super
      (String.concat "," vars)
      (String.concat "," (List.map (fun (k,_) -> k) (IdMap.bindings dict)))
      (String.concat "," subs)

let string_of_value_list (v : value list) =
  String.concat "," (List.map string_of_value v)

let is_metaclass name =
  String.get name 0 = '^'

let pretty_of_name name =
  if is_metaclass name then
    let name' = Str.global_replace (Str.regexp "\\^") "" name in
    name' ^ " class"
  else
    name

  
let pretty_of_value = function
  | VObjLoc _ -> failwith "find VObjLoc: fully eval the value before passing to this function"
  | VObject ("UndefinedObject", VNil, _) -> "nil"
  | VObject ("True", _, _) -> "true"
  | VObject ("False", _, _) -> "false"
  | VObject (_, VNum n, _) -> string_of_int n
  | VObject (_, VString s, _) -> "'" ^ s ^ "'"
  | VObject (_, VClosure (params, body, _), _) ->
    "a closure: " ^ (string_of_cexp body)
  | VObject (_, VArray arr, _) ->
    sprintf "#( %s )" (String.concat ", " (List.map string_of_value arr))
  | VObject (cls, _, _) ->
    "a(an) " ^ cls
  | VClass (_, _, _, name, _) ->
    pretty_of_name name

(* next available location, and the mapping *)
type store = loc * value LocMap.t
type result = value * genv * store

let string_of_store (store : store) : string =
  let _, sto = store in
  let s = List.map (fun (loc, v) ->
      sprintf "%3d -> %s" loc (string_of_value v))
      (LocMap.bindings sto) in
  String.concat "\n" s

let mt_env = IdMap.empty
let mt_store : store = 0, LocMap.empty
let mt_methods : methodv = IdMap.empty
let mt_subs = []

let is_VObject = function
  | VObject (_, _, _) -> true
  | _ -> false
let is_VClass = function
  | VClass _ -> true
  | _ -> false

let is_VObjLoc = function
  | VObjLoc _ -> true
  | _ -> false

exception InternalError of string
exception EvalError of string
exception PrimError of string
(* Return expression is implemented in exception *)
exception VReturn of label * value * genv * store
(* Smalltalk User Error *)
exception VError of string

let error_prim msg = raise (PrimError msg)
let error_internal msg = raise (InternalError msg)
let error_eval msg = raise (EvalError msg)
let errstr (expect : string) (got : string) : string =
  sprintf "expect a %s, but got: %s" expect got
let err_value_msg (expect : string) (got : value) : string =
  errstr expect (pretty_of_value got)
let unexpected_VObjLoc whichfunc (objv : value) =
  failwith (whichfunc ^ ": find VObjLoc " ^ (string_of_value objv))
let error_unbound id = error_eval (id ^ " is unbound")

(* helper functions for env and store *)
let lookup_env (x : string) (env : env) : loc option =
  if IdMap.mem x env then
    Some (IdMap.find x env)
  else
    None

(* decide whether x is bound in env *)
let is_bound (x : string) (env : env) : bool =
  match lookup_env x env with
  | None -> false
  | Some _ -> true

let rec lookup_store ~realobj (l : loc) (store : store) : value option =
  let _, sto = store in
  try
    match LocMap.find l sto with
    | VObjLoc loc when realobj -> lookup_store realobj loc store
    | v -> Some v
  with _ -> None
      
let fetch ~realobj (x : string) (env : env) (store : store) : value option =
  match lookup_env x env with
  | Some (l) -> lookup_store ~realobj l store 
  | None -> None

let fetch_or_fail ~realobj x genv store : value =
  match fetch ~realobj x genv store with
  | None -> error_unbound x
  | Some (v) -> v

let update_env (x : string) (l : loc) (env : env) : env =
  IdMap.add x l env
    
let update_store (l : loc) (v : value) (store : store) : store =
  let loc, sto = store in
  assert( l <= loc);
  loc, LocMap.add l v sto

let alloc (v : value) store : loc * store =
  let loc, sto = store in
  let loc' = loc + 1 in
  let store' = LocMap.add loc' v sto in
  (*printf "alloc %d for %s\n" loc' (string_of_value v);*)
  loc', (loc', store')

(* store v and bind x to a reference of v.
 * return the reference, new env and new store *)
let alloc_and_bind_ref (x : id) (v : value) env store : value * env * store =
  let loc, store' = alloc v store in
  let rv = VObjLoc loc in
  let loc2, sto = alloc rv store' in
  let env' = update_env x loc2 env in
  rv, env', sto

(* update what x is pointing to. *)
let update_bind (x : string) (v : value) env store : env * store =
  match lookup_env x env with
  | None -> failwith ("update_bind: " ^ x ^ " is not found. use add_bind")
  | Some (loc) -> env, update_store loc v store

(* if x is pointing to VObjLoc, update its original source *)
let update_bind_origin (x : string) (v : value) env store : env * store =
  let rec update_origin_loc (l : loc) : store =
    (* if the loc does not point to a VObjLoc, the loc is the original loc *)
    match lookup_store ~realobj:false l store with
    | None -> error_unbound x
    | Some (VObjLoc loc) -> update_origin_loc loc
    | Some (_) -> update_store l v store
  in
  match fetch_or_fail ~realobj:false x env store with
  | VObjLoc loc -> env, update_origin_loc loc
  | _ -> error_internal (x ^ " is not pointing to a VObjLoc")

(* allocate space for v, and simply bind x to v (so v must be VObjLoc) *)
let add_bind (x : string) (v : value) env store : env * store =
  if not (is_VObjLoc v) then
    error_internal ((string_of_value v) ^ " is not a VObjLoc")
  else
    let loc, store' = alloc v store in
    let env' = update_env x loc env in
    env', store'

(* simply bind each id of xs to each value of vs. No reference is created.
 * So, each value in vs must be ref*)
let rec add_bind_list (xs : id list) (vs : value list) env store : env * store =
  if List.for_all (fun v -> is_VObjLoc v) vs then
    match xs, vs with
    | [], [] -> env, store
    | _, []
    | [], _ -> error_eval "arity mismatch"
    | x :: tlx, v :: tlv ->
      let env', store' = add_bind x v env store in
      add_bind_list tlx tlv env' store'
  else
    error_eval ("some value are not VObjLoc: " ^ (string_of_value_list vs))

(* Return a new environment that contains only the mapping from names to
 * a nil reference. *)
let init_nil_bindings (xs : id list) genv store : env * store =
  let nilv = fetch_or_fail ~realobj:false _nil_ genv store in
  let nils = List.map (fun _ -> nilv) xs in
  add_bind_list xs nils mt_env store

let real_object ?(f="") (v : value) (store : store) : value =
  match v with
  | VObjLoc loc -> begin match lookup_store ~realobj:true loc store with
      | None ->
        dprint (string_of_store store);
        error_eval (sprintf "real_object:%s: @%d is not found in store%!"
                      f loc)
      | Some (v) -> v
    end
  | v -> v

let lookup_methodv (x : id) (dict : methodv) : cexp option =
  try Some (IdMap.find x dict) with
  | _ -> None
    
let rec lookup_methodv_follow_chain (methodname : id) (clsname : string) genv store
  : cexp option =
  dprint (sprintf "lookup method %s in %s\n%!" methodname clsname);
  if clsname = _nil_ then None
  else begin match fetch ~realobj:true clsname genv store with
    | None -> error_internal (clsname ^ " is unbound")
    | Some ( VObject (_, _, _) )->
      error_internal ("lookup_methodv_follow_chain: super is found not being a Class while searching: " ^ methodname)
    | Some (VClass (super, _, dict, _, _)) ->
      begin match lookup_methodv methodname dict with
        | Some (v) ->
          dprint ("found in " ^ clsname ^ "\n");
          Some (v)
        | None -> lookup_methodv_follow_chain methodname super genv store
      end
    | Some (VObjLoc _) -> failwith "lookup_method: unreachable"
  end

let update_methodv (x : id) (lam : cexp) (dict : methodv) : methodv =
  IdMap.add x lam dict

let raw_string (objv : value) : string = match objv with
  | VObject (_, VString s, _) -> s
  | _ -> raise (InternalError (err_value_msg "VString (in raw_string)" objv))
let raw_int (objv : value) : int = match objv with
  | VObject (_, VNum n, _) -> n
  | _ -> error_internal (err_value_msg "VNum (in raw_int)" objv)

let map_VArray (func : value -> 'a) (arr : value) : 'a list =
  match arr with
  | VObject (_, VArray elms, _) -> List.map func elms
  | _ -> raise (InternalError (err_value_msg "VArray" arr))


(* we know how to fetch the nil value *)
  
let get_nilv genv store = fetch_or_fail ~realobj:false _nil_ genv store
let get_truev genv store = fetch_or_fail ~realobj:false _true_ genv store
let get_falsev genv store = fetch_or_fail ~realobj:false _false_ genv store
    
