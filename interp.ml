open Core_syntax
open Core_value
open Eval
open Builtins
open Printf

exception ExitRequest of int

let interp (exp : cexp) : value * genv * store =
  let env = mt_env in
  let genv, store = bootstrap () in
  eval exp env genv store


let answer (exp : cexp) : value =
  let v, _, _ = interp exp in
  v

let with_error (exp : cexp) (lam : (cexp -> 'a)) : 'a =
  try
    lam exp
  with
  | Failure msg ->
    eprintf "Failure: %s\n%!" msg; raise (ExitRequest 1)
  | InternalError msg ->
    eprintf "Internal Error: %s\n%!" msg; raise (ExitRequest 2)
  | PrimError msg ->
    eprintf "Primitive Error: %s\n%!" msg; raise (ExitRequest 3)
  | VReturn _ ->
    eprintf "Error: Return out of context\n%!"; raise (ExitRequest 4)
  | VError msg ->
    eprintf "%s\n%!" (Scanf.unescaped msg); raise (ExitRequest 5)
  | EvalError msg ->
    eprintf "Evaluate Error: %s\n%!" msg;
    raise (ExitRequest 6)
  | _ -> eprintf "Unknown Error\n%!"; raise (ExitRequest 7)
  
let interp_catch_error (exp : cexp) =
  with_error exp interp

let answer_catch_error (exp : cexp) =
  with_error exp answer

let eval_catch_error (exp : cexp) (env : env) (genv : genv) (store : store) : result =
  let evaluate e = eval e env genv store in
  with_error exp evaluate
