| runner |
TestCase subclass: #Tester.
Tester compile: '

TestChangeSuper
    |x|
    Object subclass: #P1 instanceVariableNames: ''a''.
    P1 subclass: #P2 instanceVariableNames: ''b''.
    P2 subclass: #P3 instanceVariableNames: ''c''.

    P2 compile: ''setb b:=2''.

    P3 compile: ''getb ^b''.

    x := P3 new setb.

    P3 superclass: P1.

    self assert: x getb == 2.
'.
runner := Tester new.
runner TestChangeSuper.
