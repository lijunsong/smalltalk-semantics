open Core_syntax
open Core_value
open Eval
open OUnit2
open Interp

let eval_predicate (exp : cexp) (pass_func : value -> genv -> store -> bool) =
  let test test_ctx : unit =
    let v, genv, store = interp exp in
    if pass_func v genv store then ()
    else assert_failure ("fail: " ^ (string_of_cexp exp) ^ " got: " ^
                        (string_of_value v))
  in
  test

let eval_test =
  "Test the core language evaluation" >:::
  [
    "let bindings" >::
    (eval_predicate
       (Let ("x", (Id _nil_), Assign ("x", Num 1)))
       (fun v genv store ->
          let is1 = match real_object v store with
            | VObject (_, VNum 1, _) -> true
            | _ -> false in
          let isnil = match fetch ~realobj:true _nil_ genv store with
            | Some (VObject (_, VNil, _)) -> true
            | _ -> false in
          is1 && isnil));

    "let shadows 1" >::
    (eval_predicate
       (Let ("x", Num 1,
             Let ("x", Num 2,
                  Id "x")))
       (fun v genv store ->
          match real_object v store with
            | VObject (_, VNum 2, _) -> true
            | _ -> false));

    "let shadows 2" >::
    (eval_predicate
       (Let ("x", Num 1,
             Seq (Let ("x", Num 2, Assign ("x", Num 100)),
                  Id "x")))
       (fun v g s -> match real_object v s with
          | VObject (_, VNum 1, _) -> true
          | _ -> false));

    "let shadows 2" >::
    (eval_predicate
       (Let ("x", Num 1,
             Seq (Let ("y", Num 2, Assign ("x", Num 100)),
                  Id "x")))
       (fun v g s -> match real_object v s with
          | VObject (_, VNum 100, _) -> true
          | _ -> false));
    

    "lambda shadow" >::
    (eval_predicate
       (Let ("x", Num 1,
             Let ("y", App (Lam (["x"], Seq (Assign ("x", Num 2),
                                             Id "x")),
                            [Num 100]),
                  Id "x")))
       (fun v g s -> match real_object v s with
          | VObject (_, VNum 1, _) -> true
          | _ -> false
       ));
                  

    "app" >::
    (eval_predicate
       (App (Lam (["x"; "y"], Id "y"),
             [Num 1; Num 2]))
       (fun v g s -> match real_object v s with
          | VObject (_, VNum 2, _) -> true
          | _ -> false));
             
        
    "assign" >::
    (eval_predicate
       (Let ("x", (Id _nil_),
             Seq (Assign ("x", Num 1),
                  (Id "x"))))
       (fun v g s -> match real_object v s with
          | VObject (_, VNum 1, _) -> true
          | _ -> false));
       

  ]

let _ =
  run_test_tt_main eval_test
