open OUnit2
open St_syntax
open Parse

let cmp (src : string) (expect : stexp) =
  let test test_ctx =
    let result = parse_smalltalk_string src in
    assert_equal expect result
  in
  test

let parse_test =
  "Test Parsing Smalltalk Source" >:::
  [
    "parse Id" >::
    (cmp
       "Object"
       (STId "Object"));

    "parse number" >::
    (cmp "1"
       (STNum 1));

    "assign" >::
    (cmp "x := 1"
       (STAssign ("x", STNum 1)));

    "block" >::
    (cmp "[]"
       (STLam ([],
               (STSequence ([], [])))));

    "block with body only" >::
    (cmp "[ Object ]"
       (STLam ([],
               STSequence ([],
                           [STId "Object"]))));

    "block with args only" >::
    (cmp "[:x]"
       (STLam (["x"],
               STSequence ([], []))));


    "block with args" >::
    (cmp "[ :x | ^ x]"
       (STLam (["x"],
               STSequence ([],
                           [STReturn (STId "x")]))));

    "block with args and temporaries" >::
    (cmp "[ :x :y :z| | t t1 | ^ t ]"
       (STLam (["x"; "y"; "z"],
               STSequence (["t"; "t1"],
                           [STReturn (STId "t")]))));


    "parse unary message" >::
    (cmp "Object asString"
       (STSendUnary (STId "Object",
                     "asString")));

    "parse binary message" >::
    (cmp "Object + Object"
       (STSendBinary (STId "Object",
                      "+",
                      STId "Object")));

    "parse keyword message" >::
    (cmp "a ifTrue: b ifFalse: c else: d"
       (STSendKeyword (STId "a",
                       "ifTrue:ifFalse:else:",
                       [(STId "b");
                        (STId "c");
                        (STId "d")])));
     
    "parse keyword message" >::
    (cmp "a ifTrue: b"
       (STSendKeyword
          (STId "a",
           "ifTrue:",
           [STId "b"])));

    "parse nil" >::
    (cmp "nil" STNil);

    "parse self" >::
    (cmp "self" STSelf);

    "parse super" >::
    (cmp "super" STSuper);

    "parse true" >::
    (cmp "true" STTrue);

    "parse false" >::
    (cmp "false" STFalse);

    "parse array" >::
    (cmp "#(1 2 3 4)"
       (STArray [STNum 1;
                 STNum 2;
                 STNum 3;
                 STNum 4]));
  ]

let _ =
  run_test_tt_main parse_test
