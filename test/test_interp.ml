open Interp
open Parse
open Desugar
open Eval
open Core_syntax
open Core_value
open OUnit2
open Core_util
open Prims

let test_no_error_raised (src : string) =
  let test test_ctx : unit=
    let exp = desugar (parse_smalltalk_string src) in
    try
      let _ = interp exp in
      ()
    with _ -> assert_failure ("Unexpected error occur on source: \n" ^ src)
  in
  test

let test_doesNotUnderstand (src : string) =
  let test test_ctx : unit=
    let exp = desugar (parse_smalltalk_string src) in
    try
      let _ = interp exp in
      assert_failure ("expect doesNotUnderstand error occurs, but it didn't. Source: " ^ src)
    with
    | VError msg when has_substring msg "doesNotUnderstand" -> ()
    | VError msg ->
      assert_failure ("expect doesNotUnderstand error occurs, instead the error is " ^ msg)
    | _ -> assert_failure "doesNotUnderstand is not raised"
  in
  test

(* pass_func takes the evaluated value, to decide whether the value is right *)
let test_eval (src : string) (pass_func : value -> genv -> store -> bool) =
  let test test_ctx : unit =
      let v, genv, store = interp (desugar (parse_smalltalk_string src)) in
      if pass_func v genv store then ()
      else assert_failure ("fail: " ^ src ^ " got: \n" ^
                           (string_of_value v))
  in
  test


let is_true (v : value) (genv : genv) (store : store) : bool =
  same_object2 _true_ v genv store
    
let is_false v genv store : bool =
  same_object2 _false_ v genv store

let interp_test =
  "Test interpreting smalltalk source" >:::
  [
    (* test on bootstrap *)
    "no error on number" >::
    (test_no_error_raised "1. 2. 3. 4");

    "no error on string" >::
    (test_no_error_raised "'hello'. 'again'");

    "no error on block" >::
    (test_no_error_raised "[]");

    "no error on boolean" >::
    (test_no_error_raised "true. false.");

    (* message send *)
    "no error on message exists" >::
    (test_no_error_raised "1 asString");

    "no error on eval block" >::
    (test_no_error_raised "
       [ 1 ] value
       ");

    "no error on eval block" >::
    (test_no_error_raised "
       [ :arg1 | arg1 ] value: 1
       ");
    
    
    "no error on creating new subclass" >::
    (test_no_error_raised "Object subclass: #P1.");

    "no error on create subclass with instance variables" >::
    (test_no_error_raised "
       Object subclass: #P1 instanceVariableNames: 'a b c'
       ");

    "no error on create instance" >::
    (test_no_error_raised "
       Object subclass: #P1.
       P1 new.");
    
    (* Test Evaluation Result *)
    "test eval num to VObjLoc" >::
    (test_eval "1" (fun v g s -> match v with
         | VObjLoc _ -> true
         | _ -> false
       ));

     "test eval string to VObjLoc" >::
    (test_eval "'1'" (fun v g s -> match v with
         | VObjLoc _ -> true
         | _ -> false
       ));

    "test eval id to VObjLoc" >::
    (test_eval "Object" (fun v g s -> match v with
         | VObjLoc _ -> true
         | _ -> false
       ));

    "test eval nil to VObjLoc" >::
    (test_eval "nil" (fun v g s -> match v with
         | VObjLoc _ -> true
         | _ -> false
       ));

    "test eval true to VObjLoc" >::
    (test_eval "true" (fun v g s -> match v with
         | VObjLoc _ -> true
         | _ -> false
       ));

    "test eval expression to VObjLoc" >::
    (test_eval "Object subclass: #P1" (fun v g s -> match v with
         | VObjLoc _ -> true
         | _ -> false
       ));
    

    "Test eval assign" >::
    (test_eval "|x y| x := y := 1" (fun v g s -> match (real_object v s) with
         | VObject (_, VNum 1, _) -> true
         | _ -> false
       ));

    "Test eval assign" >::
    (test_eval
      "| x y |
       x := y := Object.
       x := 1.
       y."
      (fun v g s -> match (real_object v s) with
         | VClass _ -> true
         | _ -> false));

    "Test compare location 1" >::
    (test_eval "'12' == ['12'] value" (fun v g s ->
         is_false v g s));

    "Test compare location 2" >::
    (test_eval "'12' = ['12'] value" (fun v g s ->
         is_true v g s));

    "Test compare location 3" >::
    (test_eval "Object == Object" (fun v g s -> is_true v g s));

    "Test compare location 4" >::
    (test_eval "Object == [Object] value" (fun v g s -> is_true v g s));

    "Test compare location 5" >::
    (test_eval
      "| x y |
       x := y := Object new.
       x == y"
      (fun v g s -> is_true v g s));

    "Test compare location 6" >::
    (test_eval
      "| x y |
       x := Object new.
       y := Object new.
       x == y."
      (fun v g s -> is_false v g s));
    
    "Test pass by reference" >::
    (test_eval
      "| x y |
       Object subclass: #P1 instanceVariableNames: 'a'.
       P1 compile: 'seta a:=100'.
       P1 compile: 'geta ^a'.
       x := y := P1 new.
       x seta.
       y geta."
      (fun v g s -> match (real_object v s) with
         | VObject (_, VNum 100, _) -> true
         | _ -> false));

    "Test instsance varibales" >::
    (test_eval
      "Object subclass: #P1 instanceVariableNames: 'a b'.
       P1 compile: 'seta a:=9999'.
       P1 compile: 'geta ^a'.
       P1 new seta geta"
      (fun v g s -> match (real_object v s) with
         | VObject (_, VNum 9999, _) -> true
         | _ -> false));

    "Test nil is not changed" >::
    (test_eval
      "| x y |
       Object subclass: #P1 instanceVariableNames: 'a'.
       P1 compile: 'seta a:=100'.
       P1 compile: 'geta ^a'.
       x := y := P1 new.
       nil."
      (fun v g s -> same_object2 _nil_ v g s));

    "Test pass by reference 2" >::
    (test_eval
      "| x y |
       Object subclass: #P1 instanceVariableNames: 'a'.
       P1 compile: 'seta a:=100'.
       P1 compile: 'geta ^a'.
       x := P1 new.
       y := P1 new.
       x seta.
       y geta."
      (fun v g s -> same_object2 _nil_ v g s));

    "Test super" >::
    (test_eval
      "
        Object subclass: #PPP.
        PPP subclass: #PP.
        PPP compile: 'get10 ^ 10'.
        PP compile: '
        callSuper
           | super |
            ^ super get10
        '.
        PP new callSuper == 10
      "
      (fun v g s -> same_object2 _true_ v g s));

    "Test assign to super" >::
    (test_eval
      "
        Object subclass: #PPP.
        PPP subclass: #PP.
        PPP compile: 'get10 ^ 10'.
        PP compile: '
        getSuper
          | super b |
           super := 1.
            b := 2 + super.
            ^ b

        '.
        PP new getSuper.
      "
      (fun v g s -> match real_object v s with
         | VObject (_, VNum 3, _) -> true
         | _ -> false));

    "Test: super assignment and receive" >::
    (test_doesNotUnderstand
      "
        Object subclass: #PPP.
        PPP subclass: #PP.
        PPP compile: 'get10 ^ 10'.
        PP compile: '
        getSuper
           \" message does not understand PPP >> +\"
           | super b |
           super := 1.
           b := super + 2.
        '.

        PP new getSuper.
      ");
    
  ]

let _ =
  run_test_tt_main interp_test
