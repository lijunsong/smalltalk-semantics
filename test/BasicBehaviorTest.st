| runner |

TestCase subclass: #BasicBehaviorClassMetaclassTest.

BasicBehaviorClassMetaclassTest compile: '

testBehaviorClassClassDescriptionMetaclassHierarchy
	"Test classes"

	self assert: Class superclass  == ClassDescription.
	self assert: Metaclass superclass == ClassDescription.

	self assert: ClassDescription superclass  == Behavior.
	self assert: Behavior superclass  = Object.

	self assert: Class class class ==  Metaclass.
	self assert: Metaclass class class  == Metaclass.
	self assert: ClassDescription class class == Metaclass.
	self assert: Behavior class class == Metaclass.

'.

BasicBehaviorClassMetaclassTest compile: '

testMetaclass
	self assert: Number class class == Metaclass.
	self assert: Behavior class class == Metaclass.
	self assert: Object class class == Metaclass.

'.

BasicBehaviorClassMetaclassTest compile: '

testMetaclassName
	self assert: Object class name = ''Object class''.
	self assert: Object class class class name = ''Metaclass class''.

'.

BasicBehaviorClassMetaclassTest compile: '

testMetaclassSuperclass
	self assert: String class superclass == String superclass class.
	self assert: TestCase class superclass == TestCase superclass class.

'.

BasicBehaviorClassMetaclassTest compile: '

testSuperclass
        self assert: Object superclass == nil.
'.

runner := BasicBehaviorClassMetaclassTest new.

runner testBehaviorClassClassDescriptionMetaclassHierarchy.
runner testMetaclass.
runner testMetaclassName.
runner testMetaclassSuperclass.
runner testSuperclass.
