| runner |

Object subclass: #One.
One compile: 'test ^1'.
One compile: 'result1 ^self test'.

One subclass: #Two.
Two compile: 'test ^2'.

Two subclass: #Three.
Three compile: 'result2 ^self result1'.
Three compile: 'result3 ^super test'.

Three subclass: #Four.
Four compile: 'test ^4'.

TestCase subclass: #Tester.
Tester compile: '
testSelf
    | example1 example2 |
    example1 := One new.
    example2 := Two new.
    self assert: example1 test == 1.
    self assert: example1 result1 == 1.
    self assert: example2 test == 2.
    self assert: example2 result1 == 2.
'.

Tester compile: '
testSuper
    | example3 example4 |
    example3 := Three new.
    example4 := Four new.

    self assert: example3 test == 2.
    self assert: example4 result1 == 4.
    self assert: example3 result2 == 2.
    self assert: example4 result2 == 4.
    self assert: example3 result3 == 2.
    self assert: example4 result3 == 2.
'.

Tester compile: '
testSuper2
   self == super
'.

runner := Tester new.
runner testSelf.
runner testSuper.
runner testSuper2.
