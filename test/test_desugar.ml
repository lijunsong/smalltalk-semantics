open OUnit2
open St_syntax
open Core_syntax
open Desugar

let cmp (src : stexp) (expect : cexp) =
  let test test_ctx =
    let result = desugar src in
    assert_equal expect result
  in
  test

(* apply p to the result to decide whether it is passed the test *)
let cmp_predicate (src : stexp) (p : cexp->bool) =
  let test test_ctx =
    let result = desugar src in
    assert_equal true (p result)
  in
  test

let desugar_test =
  "Test Desugaring" >:::
  [
    "desugar Id" >::
    (cmp
       (STId "Object")
       (Id "Object"));

    "desugar number" >::
    (cmp
       (STNum 1) (Num 1));

    "assign" >::
    (cmp
       (STAssign ("x", STNum 1))
       (Assign ("x", Num 1)));

    "block: nil at the end" >::
    (cmp
       (STLam ([],
               (STSequence ([], []))))
       (Lam ([], Id _nil_)));


    "block with body only" >::
    (cmp
       (STLam ([],
               STSequence ([],
                           [STId "Object"])))
       (Lam ([], Id "Object")));

    "block with args only" >::
    (cmp
       (STLam (["x"],
               STSequence ([], [])))
       (Lam (["x"], Id _nil_)));


    "block with args" >::
    (cmp_predicate
       (STLam (["x"],
               STSequence ([],
                           [STReturn (STId "x")])))
       (fun result -> match result with
          | Lam (["x"], Return (_, Id "x")) -> true
          | _ -> false));

    "block with args and temporaries" >::
    (cmp_predicate
       (STLam (["x"; "y"; "z"],
               STSequence (["t"; "t1"],
                           [STReturn (STId "t")])))
       (fun result -> match result with
          | Lam (["x"; "y"; "z"],
                 Let ("t", Id nil1,
                      Let ("t1", Id nil2,
                           Return (_, Id "t")))) ->
            nil1 = _nil_ && nil2 = _nil_
          | _ -> false));

    "desugar keyword message" >::
    (cmp_predicate
       (STSendKeyword (STId "a",
                       "ifTrue:ifFalse:else:",
                       [(STId "b");
                        (STId "c");
                        (STId "d")]))
       (fun e -> match e with
          | Let (r1, Id "a",
                 Let (cls1, Prim ("class", [Id r2]),
                      Lookup ("ifTrue:ifFalse:else:", Id cls2,
                              Id r3, [Id "b"; Id "c"; Id "d"]))) ->
            r1 = r2 && r2 = r3 && cls1 = cls2
          | _ -> false));
   
    "desugar nil" >::
    (cmp STNil (Id _nil_));

    "desugar self" >::
    (cmp STSelf (Id _self_));

    "desugar super" >::
    (cmp STSuper (Id _super_lit_));


    "desugar array" >::
    (cmp
       (STArray [STNum 1;
                 STNum 2;
                 STNum 3;
                 STNum 4])
       (Array [Num 1;
               Num 2;
               Num 3;
               Num 4]));

  ]

let _ =
  run_test_tt_main desugar_test
