open St_syntax
open Core_syntax

let fresh_name name =
  name
let cur_label = ref 0
let new_label () : unit =
  cur_label := !cur_label + 1

(* 1. bind _super_ at the beginning
 * 2. add return self at the end. *)
let elaborate_method (curr_class : id) (seq : stexp) : stexp =
  let super_bind =
    [STAssign (_super_, STPrim ("superclass", [STId curr_class]));
     STAssign (_super_lit_, STId _self_)]
  in
  match seq with
  | STSequence (t, stmts) ->
    let new_temps = IdSet.elements (IdSet.of_list (_super_ :: _super_lit_ :: t)) in
    STSequence (new_temps,
                super_bind @ (stmts @ [ STReturn STSelf ]))
  | _ -> failwith ("expected a STSequence, but got " ^ (string_of_stexp seq))

(* when auto_raise is true, all undeclared identifiers will be
 * automatically declared by let bindings. When desugaring a method,
 * auto_raise must be off to avoid to re-declare instance variables. *)
let rec desugar ?(auto_raise=true) (exp : stexp) : cexp =
  match exp with
  | STId id -> Id id
  | STNum n -> Num n
  | STString s -> String s
  | STNil -> Id _nil_
  | STSelf -> Id _self_
  (* normal use of super is just an identifier, bound to self *)
  | STSuper -> Id _super_lit_
  | STTrue -> Id _true_
  | STFalse -> Id _false_
  | STSendUnary (receiver, selector) ->
    desugar_send receiver selector []
  | STSendBinary (receiver, selector, arg) ->
    desugar_send receiver selector [arg]
  | STSendKeyword (reciever, selector, args) ->
    desugar_send reciever selector args
  | STSequence (temps, stmts) ->
    let temps' =
      if auto_raise then
        let undeclared_temps = IdSet.elements (undeclared_vars exp) in
        temps @ undeclared_temps
      else
        temps
    in
    (* if the stmts is an empty list, insert Nil in the end *)
    let stmts' = match stmts with
      | [] -> [STNil]
      | _ -> stmts in
    let dsg_stmts : cexp = desugar_stmts stmts' in
    desugar_temporaries temps' dsg_stmts
  | STAssign (x, e) ->
    Assign (x, desugar e)
  | STLam (xs, body) ->
    let body' = desugar body in
    Lam (xs, body')
  | STApp (f, args) ->
    App (desugar f, List.map desugar args)
  | STArray elms ->
    Array (List.map desugar elms)
  | STReturn e ->
    Return (!cur_label, desugar e)
  | STPrim (op, args) ->
    Prim (op, List.map desugar args)
  | STMethod (cls, pattern, params, body) ->
    new_label ();
    (* 1. add return self at the end of body
     * 2. add WithLabel at the beginning of body 
     * 3. add self in parameter *)
    let newbody = elaborate_method cls body in
    let dsg_body = desugar ~auto_raise:false newbody in
    let body' = WithLabel (!cur_label, dsg_body) in
    let params' = _self_ :: params in
    let lam = Lam (params', body') in
    Prim ("new-method", [String cls;
                         String pattern;
                         lam])
and undeclared_vars (exp : stexp) : IdSet.t =
  match exp with
  | STSequence (vars, stmts) ->
    let used_vars : IdSet.t list = List.map undeclared_vars stmts in
    let used_vars_set = List.fold_left (fun set elm ->
        IdSet.union set elm) IdSet.empty used_vars in
    IdSet.diff used_vars_set (IdSet.of_list vars)
  | STAssign (x, e) ->
    IdSet.union (IdSet.singleton x) (undeclared_vars e)
  | _ -> IdSet.empty
           
and desugar_send (receiver : stexp) (mthod : string) (args : stexp list) : cexp =
  let recv_name = fresh_name "recv" in
  let cls_name = fresh_name "cls" in
  let argsv = List.map desugar args in
  match receiver with
  | STSuper ->
    Let (recv_name, Id _self_,
         Let (cls_name, Id _super_,
              Lookup (mthod, Id cls_name, Id recv_name, argsv)))
  | _ ->
    Let (recv_name, (desugar receiver),
         Let (cls_name, Prim ("class", [Id recv_name]),
              Lookup (mthod, Id cls_name, Id recv_name,
                      argsv)));

and desugar_stmts (stmts : stexp list) : cexp =
  match stmts with
  | [] -> failwith "Empty statement is not allowed"
  | hd :: [] -> desugar hd
  | hd :: tl ->
    Seq (desugar hd, desugar_stmts tl)

and desugar_temporaries (temps : string list) (body : cexp) : cexp =
  match temps with
  | [] -> body
  | hd :: tl ->
    Let (hd, Id _nil_, desugar_temporaries tl body)

