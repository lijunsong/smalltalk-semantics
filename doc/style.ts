<TeXmacs|1.99.2>

<style|source>

<\body>
  <assign|to|<macro|left|right|<arg|left><long-arrow|\<rubber-rightarrow\>|\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>><arg|right>>>

  <assign|ds|<macro|arg|\<cal-D\>\<cal-S\><around|\<llbracket\>|<arg|arg>|\<rrbracket\>>>>

  <assign|st|<macro|<name|SmallTalk>>>
</body>

<\initial>
  <\collection>
    <associate|page-type|letter>
  </collection>
</initial>