<TeXmacs|1.99.2>

<style|<tuple|generic|style.ts>>

<\body>
  <section|Syntax and Value Domain of the Core Language>

  <\strong>
    Syntax
  </strong>

  <big-figure|<tabular|<tformat|<table|<row|<cell|<math|e>>|<cell|:=>|<cell|>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|Id>
  <em|x>>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|Num>
  <em|n>>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|String>
  <em|s>>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|Let> <em|x> <em|e>
  <em|e>>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|Seq> <em|e
  e>>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|Assign> <em|x>
  <em|e>>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|Lam> <math|x<rsup|\<ast\>>>
  <em|e>>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|Prim> <em|x>
  <math|e<rsup|\<ast\>>>>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|WithLabel>
  <em|n> <em|e>>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|Return> <em|n
  e>>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|Lookup> <math|I<rsub|method>>
  <em|e> <em|e> <math|e<rsup|\<ast\>>>>>|<row|<cell|>|<cell|>|<cell|>>|<row|<cell|<em|x>>|<cell|:=>|<cell|String
  Literal>>|<row|<cell|<math|n>>|<cell|:=>|<cell|Integer
  Literal>>>>>|Expressions of the core language>

  \;

  <strong|Values><\footnote>
    Actually the implementation does not have <verbatim|VReturn> as one of
    its value. <verbatim|VReturn> is implemented using <name|Ocaml>'s
    exception.
  </footnote>

  <big-figure|<tabular|<tformat|<table|<row|<cell|<math|l>>|<cell|:=>|<cell|<em|locations>>|<cell|>>|<row|<cell|<math|v>>|<cell|:=>|<cell|<em|l>>|<cell|>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|VClass>
  <math|x> <math|x<rsup|\<ast\>>> <math|d> <math|x>
  <math|x<rsup|\<ast\>>>>|<cell|>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|VObject>
  <em|x pv ><math|\<varepsilon\>>>|<cell|>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|VReturn
  ><math|v>>|<cell|>>|<row|<cell|<math|p\<nosymbol\>v>>|<cell|:=>|<cell|<verbatim|VClosure>
  <math|x<rsup|\<ast\>>> <em|e> <math|\<varepsilon\>>>|<cell|>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|VNum>
  <math|n>>|<cell|>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|VString>
  <math|s>>|<cell|>>|<row|<cell|>|<cell|\|>|<cell|<verbatim|VNil>>|<cell|>>|<row|<cell|<math|d>>|<cell|:=>|<cell|<verbatim|{>
  <math|x<rsub|1>> : <math|l<rsub|1>>; <math|\<cdots\>>; <math|x<rsub|n>> :
  <math|l<rsub|n>> <verbatim|}>>|<cell|Dictionary>>|<row|<cell|<math|\<sigma\>>>|<cell|:=>|<cell|<em|l
  : v>; <math|\<sigma\>> \| <verbatim|{}>>|<cell|Store>>|<row|<cell|<math|\<varepsilon\>>>|<cell|:=>|<cell|<em|x
  : l;> <math|\<varepsilon\>> \| <verbatim|{}>>|<cell|Environment>>|<row|<cell|<math|\<varepsilon\><rsub|g>>>|<cell|:=>|<cell|<em|x
  : l>; <math|\<varepsilon\><rsub|g>> \| <verbatim|{}>>|<cell|Global
  Environment>>|<row|<cell|<em|err>>|<cell|:=>|<cell|<verbatim|VError>
  <math|x>>|<cell|>>>>>|Values>

  \;

  <section|Evaluation Rules>

  <math|\<rightarrow\>> specifies the evaluation rules of expressions. Each
  evaluation rule takes an expression <math|e>, an environment
  <math|\<varepsilon\>>, a global environment <math|\<varepsilon\><rsub|g>>,
  and a store <math|\<sigma\>>, and evaluates the expression into a value
  <math|v>, an updated global environment
  <math|\<varepsilon\><rsub|g><rprime|'>> and an updated store
  <math|\<sigma\><rprime|'>>, denoted as

  <\equation*>
    e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>
  </equation*>

  <math|<long-arrow|\<rubber-rightarrow\>|\<ast\>>>specifies the rules of
  evaluations a list of expressions, left to right. The updated global
  environment and store are passed down during the evaluation.

  <math|\<Rightarrow\>> specifies the rules of the construction of an object.
  Constructing objects needs the class hierarchy information, and thus needs
  the global environment and store.

  <\equation*>
    e,\<varepsilon\><rsub|g>,\<sigma\>\<Rightarrow\>v,\<sigma\><rprime|'>
  </equation*>

  <strong|Evaluation Relations>

  <st> passes objects by reference. Number, String and Lam are all evaluated
  to a reference (location) to a <verbatim|VObject>, later in <name|[Prim]>,
  new subclasses and new objects are also evaluated to a reference.

  <name|[Num]>

  <math|<dfrac|<math-tt|Num >n,\<varepsilon\><rsub|g>,\<sigma\>\<Rightarrow\>v,\<sigma\><rprime|'>|<math-tt|Num
  >n,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>l,\<varepsilon\><rsub|g>,\<sigma\><rprime|'>>>

  where <math|\<sigma\><rprime|'><around|(|l|)>=v>, and
  <math|v=><verbatim|VObject "SmallInteger" (VNum ><math|n><verbatim|)>
  <verbatim|{}>

  \;

  <name|[String]>

  <math|<dfrac|<math-tt|String> s,\<varepsilon\><rsub|g>,\<sigma\>\<Rightarrow\>v,\<sigma\><rprime|'>|<math-tt|String
  >s,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>l,\<varepsilon\><rsub|g>,\<sigma\><rprime|'>>>

  where <math|\<sigma\><rprime|'><around|(|l|)>=v>, and
  <math|v=><verbatim|VObject "ByteString" (VString ><math|s><verbatim|) {}>

  \;

  <name|[Lam]>

  <math|<dfrac|<math-tt|Lam >x<rsup|\<ast\>>
  e,\<varepsilon\><rsub|g>,\<sigma\>\<Rightarrow\>v,\<sigma\><rprime|'>|<math-tt|Lam
  >x<rsup|\<ast\>> e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>l,\<varepsilon\><rsub|g>,\<sigma\><rprime|'>>>

  where <math|\<sigma\><rprime|'><around|(|l|)>=v>, and
  <math|v=><verbatim|VObject "BlockClosure" (VClosure ><math|x<rsup|\<ast\>>
  e> <verbatim|{}) {}>

  \;

  <name|[Id-Local]>

  <math|><math|<math-tt|Id >x,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v,\<varepsilon\><rsub|g>,\<sigma\>>
  if <math|\<varepsilon\><around|(|x|)>=l> and
  <math|\<sigma\><around|(|l|)>=v>

  \;

  <name|[Id-Global]>

  <math|><math|<math-tt|Id >x,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v,\<varepsilon\><rsub|g>,\<sigma\>>
  if <math|x\<nin\>domain<around|(|\<varepsilon\>|)>> and
  <math|\<varepsilon\><rsub|g><around|(|x|)>=l>,
  <math|\<sigma\><around|(|l|)>=v>

  \;

  [<name|Id-NotFound>]

  <math|><math|<math-tt|Id >x,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v<rsub|nil>,\<varepsilon\><rsub|g>,\<sigma\>>
  if <math|x\<nin\>domain<around|(|\<varepsilon\>|)>> and
  <math|x\<nin\>domain<around|(|\<varepsilon\><rsub|g>|)>>,
  <math|v<rsub|nil>> is the instance of ``UndefinedObject,'' i.e.
  <verbatim|nil>.

  \;

  <name|[Seq]>

  <math|<dfrac|e<rsub|1>,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v<rsub|1>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'><math-tt|
  ; >e<rsub|2>,\<varepsilon\>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>\<rightarrow\>v<rsub|2>,\<varepsilon\><rsub|g><rprime|''>,\<sigma\><rprime|''>|<math-tt|Seq
  >e<rsub|1> e<rsub|2>,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v<rsub|2>,\<varepsilon\><rsub|g><rprime|''>,\<sigma\><rprime|''>>>

  \;

  <name|[Let]>

  <math|<dfrac|e<rsub|1>,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v<rsub|1>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'><math-tt|
  ; >e<rsub|2>,x:l;\<varepsilon\>,\<varepsilon\><rsub|g><rprime|'>,l
  :v<rsub|1>;\<sigma\><rprime|'>\<rightarrow\>v<rsub|2>,\<varepsilon\><rsub|g><rprime|''>,\<sigma\><rprime|''>|<math-tt|Let
  >x<math-tt| >e<rsub|1><math-tt| >e<rsub|2>,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v<rsub|2>,\<varepsilon\><rsub|g><rprime|''>,\<sigma\><rprime|''>>>

  \;

  <name|[Assign-Local]>

  <math|<dfrac|e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>|<math-tt|Assign
  >x<math-tt| >e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|''>>>,
  where <math|\<varepsilon\><around|(|x|)>=l>;
  <math|\<sigma\><rprime|''>=l:v;\<sigma\><rprime|'>>

  \;

  <name|[Assign-Global]>

  <math|<dfrac|e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>|<math-tt|Assign
  >x<math-tt| >e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|''>>>,
  where <math|x\<nin\>domain<around|(|\<varepsilon\>|)>>;
  <math|\<varepsilon\><rsub|g><around|(|x|)>=l>;
  <math|\<sigma\><rprime|''>=l:v;\<sigma\><rprime|'>>

  \;

  <name|[Lookup]>

  <verbatim|lookup> searches the given method name in the class's method
  dictionary. If the method is found, construct a closure and apply the
  closure to arguments. The environment of the closure is either empty or
  from the environment stored in the <verbatim|VObject>. The environment is
  the key for resolving the locations of instance variables. This rule uses
  auxiliary functions <em|lookup-method> and <em|apply>.

  <em|lookup-method> searches the method name in the passed-in class. If the
  method name is not found in current class, it recursively searches in the
  super class, and terminates in class <verbatim|Object>. If the method name
  is not found anywhere, <em|lookup-method> returns the <verbatim|nil> Object
  as a searching result. If the method name is found, return a
  <verbatim|BlockClosure> whose environment is taken from the environment of
  the receiver (if the receiver is a <verbatim|VObject>), or is an empty
  environment (if the receiver is a <verbatim|VClass>).

  <em|apply> just binds parameters and arguments in the local environment,
  and evaluate the body with this local environment.

  <name|[Lookup-NotFound]>

  If the method is not found in the class chains, search
  <verbatim|doesNotUnderstand> in the receiver's class, with the original
  message as the argument.

  <tabular*|<tformat|<twith|table-width|1par>|<twith|table-hmode|exact>|<table|<row|<cell|>|<cell|<math|e<rsub|class>,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v<rsub|cls>,\<varepsilon\>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>><math|<math-tt|;>>>|<cell|>>|<row|<cell|>|<cell|<math|e<rsub|recv>,\<varepsilon\>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>\<rightarrow\>v<rsub|recv>,\<varepsilon\>,\<varepsilon\><rsub|g><rprime|''>,\<sigma\><rprime|''>><math|<math-tt|;>>>|<cell|>>|<row|<cell|>|<cell|<math|e<rsup|\<ast\>>,\<varepsilon\>,\<varepsilon\><rsub|g><rprime|''>,\<sigma\><rprime|''><long-arrow|\<rubber-rightarrow\>|\<ast\>>v\<nocomma\>s<rsup|*\<ast\>>,\<varepsilon\>,\<varepsilon\><rsub|g><rprime|'''>,\<sigma\><rprime|'''>><math|<math-tt|;>>>|<cell|>>|<row|<cell|>|<cell|<em|lookup-method>
  <math|v<rsub|cls>> <math|x<rsub|method>> <math|v<rsub|recv>>
  <math|\<varepsilon\><rsub|g><rprime|'''>>
  <math|\<sigma\><rprime|'''>\<rightarrow\>v<rsub|nil>,\<varepsilon\><rsub|g
  1>,\<sigma\><rsub|1>>>|<cell|>>|<row|<cell|>|<cell|<verbatim|Lookup>
  <verbatim|"doesNotUnderstand"> <math|e<rsub|class>> <math|e<rsub|recv>>
  [<verbatim|String ><math|x<rsub|method>>]<math|,\<varepsilon\><rsub|g1>,\<sigma\><rsub|1>\<rightarrow\>v<rprime|'>>,<math|\<varepsilon\><rsub|g2>,\<sigma\><rsub|2>>
  >|<cell|>>|<row|<cell|>|<cell|<math|<dfrac|<math-tt|
  \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ >|>>>|<cell|>>|<row|<cell|>|<cell|<math|<math-tt|Lookup
  >x<rsub|method><math-tt| >e<rsub|class><math-tt| >e<rsub|recv><math-tt|
  >e<rsup|\<ast\>>,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v<rprime|'>,\<varepsilon\><rsub|g2>,\<sigma\><rsub|2>>>|<cell|>>>>>

  <name|[Lookup-Found]>

  If the method is found in the class chains, apply the returned closure to
  the arguments that are elaborated in a way that receiver is the first
  argument that is going to bound to <strong|self>.

  <tabular*|<tformat|<twith|table-width|1par>|<twith|table-hmode|exact>|<table|<row|<cell|>|<cell|<math|e<rsub|class>,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v<rsub|cls>,\<varepsilon\>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>><math|<math-tt|;>>>|<cell|>>|<row|<cell|>|<cell|<math|e<rsub|recv>,\<varepsilon\>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>\<rightarrow\>v<rsub|recv>,\<varepsilon\>,\<varepsilon\><rsub|g><rprime|''>,\<sigma\><rprime|''>><math|<math-tt|;>>>|<cell|>>|<row|<cell|>|<cell|<math|e<rsup|\<ast\>>,\<varepsilon\>,\<varepsilon\><rsub|g><rprime|''>,\<sigma\><rprime|''><long-arrow|\<rubber-rightarrow\>|\<ast\>>v\<nocomma\>s<rsup|\<ast\>>,\<varepsilon\>,\<varepsilon\><rsub|g><rprime|'''>,\<sigma\><rprime|'''>><math|<math-tt|;>>>|<cell|>>|<row|<cell|>|<cell|<em|lookup-method>
  <math|v<rsub|cls>> <math|x<rsub|method>> <math|v<rsub|recv>>
  <math|\<varepsilon\><rsub|g><rprime|'''>>
  <math|\<sigma\><rprime|'''>\<rightarrow\>v<rsub|closure>,\<varepsilon\><rsub|g
  1>,\<sigma\><rsub|1>>>|<cell|>>|<row|<cell|>|<cell|<em|apply>
  <math|v<rsub|closure>> [<math|v<rsub|recv>>;<math|v\<nocomma\>s<rsup|\<ast\>>>]
  <math|\<varepsilon\><rsub|g1>> <math|\<sigma\><rsub|1>>
  <math|\<rightarrow\>v<rprime|'>>,<math|\<varepsilon\><rsub|g2>,\<sigma\><rsub|2>>
  >|<cell|>>|<row|<cell|>|<cell|<math|<dfrac|<math-tt|
  \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ >|>>>|<cell|>>|<row|<cell|>|<cell|<math|<math-tt|Lookup
  >x<rsub|method><math-tt| >e<rsub|class><math-tt| >e<rsub|recv><math-tt|
  >e<rsup|\<ast\>>,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v<rprime|'>,\<varepsilon\><rsub|g2>,\<sigma\><rsub|2>>>|<cell|>>>>>

  \;

  <name|[Return]>

  <math|<dfrac|e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>|<math-tt|Return
  >n<math-it| >e\<rightarrow\><math-tt|VReturn >n<math-it|
  >v,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>>>

  All the rules above need to be modified accordingly for this rule: once an
  expression is evaluated to <verbatim|VReturn>, use the <verbatim|VReturn>,
  global environment and store as its value, which prevent further
  evaluations. One example is the <name|[Seq]> rule:

  <math|<dfrac|e<rsub|1>,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\><math-tt|VReturn
  >n<math-it| >v<rsub|1>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>|<math-tt|Seq
  >e<rsub|1> e<rsub|2>,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\><math-tt|VReturn
  >n<math-it| >v<rsub|1>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>>>
  <name|[Updated-Seq]>

  \;

  <name|[With-Label]>

  <math|<dfrac|e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\><math-tt|VReturn
  >n<rsub|1><math-it| v>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>
  |<math-tt|WithLabel> n<math-it| >e\<rightarrow\>v,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>>>,
  where <math|n<rsub|1>=n>.

  \;

  <paragraph*|Comments><name|[Return]> and <name|[With-Label]> are sufficient
  to model the non-local return because every method is elaborated to have
  <verbatim|WithLabel> at the entry point and contain at least one
  <verbatim|Return> in the body. The label used in a method is unique for
  each method.

  \;

  <name|[Prim]>

  <math|<math-tt|Prim >x<math-it| >e<rsup|\<ast\>>,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>v,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>>

  Primitives are used extensively in the desugared programs. The following
  rules are for primitives that specify how classes and instances are
  created, and how to fetch particular information from an instance or a
  class.

  <name|[Prim-New-VObject]>

  given a class <em|class>, this primitive creates a new instance of the
  <em|class>.

  <tabular*|<tformat|<twith|table-width|1par>|<twith|table-hmode|exact>|<table|<row|<cell|>|<cell|>|<cell|>>|<row|<cell|>|<cell|<math|e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>l,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>>,
  where <math|\<sigma\><rprime|'><around|(|l|)>=><verbatim|VClass>
  <math|x<rsub|super>> <math|x<rsub|vars><rsup|\<ast\>>> <math|d>
  <math|x<rsub|name>> <math|x<rsup|\<ast\>><rsub|subclasses>>>|<cell|>>|<row|<cell|>|<cell|<em|vars>
  = <em|all-instance-variables ><math|\<sigma\><rprime|'><around|(|l|)>>>|<cell|>>|<row|<cell|>|<cell|<math|\<varepsilon\><rsub|obj>=><verbatim|{><em|var>
  : <verbatim|nil> <verbatim|\|> <em|var><math|\<in\>><em|vars><verbatim|}>>|<cell|>>|<row|<cell|>|<cell|<math|<dfrac|<math-tt|
  \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ >|>>>|<cell|>>|<row|<cell|>|<cell|<verbatim|Prim
  "new-vobject" ><math|e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>>
  <math|\<Rightarrow\>> <math|l<rprime|'>,\<varepsilon\><rprime|'><rsub|g>,\<sigma\><rprime|''>>>|<cell|>>|<row|<cell|>|<cell|where,
  <math|\<sigma\><rprime|''>=l<rprime|'>:><verbatim|VObject>
  <math|x<rsub|name>> <verbatim|VNil> <math|\<varepsilon\><rsub|obj>>;<math|\<sigma\><rprime|'>>>|<cell|>>>>>

  \;

  <em|all-instance-variables> traverses the class hierarchy to collect names
  of all the instance variables.

  <name|[Prim-New-Method]>

  <tabular*|<tformat|<twith|table-width|1par>|<twith|table-hmode|exact>|<table|<row|<cell|>|<cell|>|<cell|>>|<row|<cell|>|<cell|<math|e<rsub|class>,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>l,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>>,
  where <math|\<sigma\><rprime|'><around|(|l|)>=><verbatim|VClass>
  <math|x<rsub|super>> <math|x<rsub|vars><rsup|\<ast\>>> <math|d>
  <math|x<rsub|name>> <math|x<rsup|\<ast\>><rsub|subclasses>>>|<cell|>>|<row|<cell|>|<cell|<math|e<rsub|name>,\<varepsilon\>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>\<rightarrow\>l<rsub|name>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>>,
  where <math|\<sigma\><rprime|'><around|(|l<rsub|name>|)>=><verbatim|VObject
  "ByteString" ><math|x<rsub|method>> <verbatim|{}>
  >|<cell|>>|<row|<cell|>|<cell|<math|e<rsub|closure>,\<varepsilon\>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>\<rightarrow\>l<rsub|closure>,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>>,
  where <math|\<sigma\><rprime|'><around|(|l<rsub|closure>|)>=><verbatim|VObject
  "BlockClosure" ><em|pv> <verbatim|{}>>|<cell|>>|<row|<cell|>|<cell|and
  <em|pv>=<verbatim|VClosure ><math|x<rsub|params><rsup|\<ast\>>>
  <math|e<rsub|body>> <math|\<varepsilon\><rsub|closure>>>|<cell|>>|<row|<cell|>|<cell|<math|<dfrac|<math-tt|
  \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ >|>>>|<cell|>>|<row|<cell|>|<cell|<verbatim|Prim
  "new-method"> <math|e<rsub|class > e<rsub|name > e<rsub|closure
  >,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>> <math|\<rightarrow\>>
  <math|v<rsub|nil>,\<varepsilon\><rprime|'><rsub|g>,\<sigma\><rprime|''>>>|<cell|>>|<row|<cell|>|<cell|where,
  <math|\<sigma\><rprime|''><around|(|l|)>=><verbatim|VClass>
  <math|x<rsub|super>> <math|x<rsub|vars><rsup|\<ast\>>> <math|d<rprime|'>>
  <math|x<rsub|name>> <math|x<rsup|\<ast\>><rsub|subclasses>>
  >|<cell|>>|<row|<cell|>|<cell|and <math|d<rprime|'>=x<rsub|method>:><verbatim|Lam>
  <math|x<rsub|params><rsup|\<ast\>>> <math|e<rsub|body>>;<math|d>>|<cell|>>>>>

  <name|[Prim-New-Subclass]>

  given a class <em|baseclass>, the subclass name, and the names of instance
  variables, this primitive creates a subclass of the <em|baseclass>. The
  metaclass of the subclass is also created implicitly. In <st>, the
  metaclass is not namable, but in the core language, meta-classes have the
  name convention that has the same name as their instance (classes) but with
  prefix <verbatim|^> , and the class of a class that has such a prefix is
  <verbatim|Metaclass>. That is, <verbatim|Object>'s class is named
  <verbatim|^Object>, and <verbatim|^object>'s class is <verbatim|Metaclass>.
  Of course, the class of <verbatim|Metaclass> is <verbatim|^Metaclass>,
  whose class is <verbatim|Metaclass>.

  For this primitive, this document is not going to write down its evaluation
  rules, as the writing would be unreadable. It is actually more clear
  reading the code for this primitive.\ 

  <name|[Prim-Class]>

  This primitive will answer a class for a given object, or a metaclass for a
  class. When the argument is a class, and the class is not a metaclass, the
  primitive just fetch the metaclass by lookup its metaclass's name, i.e. the
  class name prefixed by <verbatim|^>:

  <tabular*|<tformat|<twith|table-width|1par>|<twith|table-hmode|exact>|<table|<row|<cell|>|<cell|>|<cell|>>|<row|<cell|>|<cell|<math|e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>l,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>>,
  where <math|\<sigma\><rprime|'><around|(|l|)>=><verbatim|VClass>
  <math|x<rsub|super>> <math|x<rsub|vars><rsup|\<ast\>>> <math|d>
  <math|x<rsub|name>> <math|x<rsup|\<ast\>><rsub|subclasses>>>|<cell|>>|<row|<cell|>|<cell|<math|\<varepsilon\><rsub|g><rprime|'>>=<math|\<cdots\>>;<verbatim|^><math|x<rsub|name>>:<math|l<rprime|'>>;<math|\<cdots\>>
  \ \ \ \ <math|\<sigma\><rprime|'><around|(|l<rprime|'>|)>=v>>|<cell|>>|<row|<cell|>|<cell|<math|<dfrac|<math-tt|
  \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ >|>>>|<cell|>>|<row|<cell|>|<cell|<verbatim|Prim
  "class" ><math|e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>>
  <math|\<Rightarrow\>> <math|v,\<varepsilon\><rprime|'><rsub|g>,\<sigma\><rprime|'>>>|<cell|>>>>>

  If the class is a metaclass, the <verbatim|Metaclass> should be returned.

  <tabular*|<tformat|<twith|table-width|1par>|<twith|table-hmode|exact>|<table|<row|<cell|>|<cell|>|<cell|>>|<row|<cell|>|<cell|<math|e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>l,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>>,
  where <math|\<sigma\><rprime|'><around|(|l|)>=><verbatim|VClass>
  <math|x<rsub|super>> <math|x<rsub|vars><rsup|\<ast\>>> <math|d>
  <verbatim|^><math|x<rsub|name>> <math|x<rsup|\<ast\>><rsub|subclasses>>>|<cell|>>|<row|<cell|>|<cell|<math|\<varepsilon\><rsub|g><rprime|'>>=<math|\<cdots\>>;<verbatim|Metaclass>:<math|l<rprime|'>>;<math|\<cdots\>>
  \ \ \ \ <math|\<sigma\><rprime|'><around|(|l<rprime|'>|)>=v>>|<cell|>>|<row|<cell|>|<cell|<math|<dfrac|<math-tt|
  \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ >|>>>|<cell|>>|<row|<cell|>|<cell|<verbatim|Prim
  "class" ><math|e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>>
  <math|\<Rightarrow\>> <math|v,\<varepsilon\><rprime|'><rsub|g>,\<sigma\><rprime|''>>>|<cell|>>>>>

  If the argument is just a normal object, simply get its class:

  <tabular*|<tformat|<twith|table-width|1par>|<twith|table-hmode|exact>|<table|<row|<cell|>|<cell|>|<cell|>>|<row|<cell|>|<cell|<math|e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>\<rightarrow\>l,\<varepsilon\><rsub|g><rprime|'>,\<sigma\><rprime|'>>,
  where <math|\<sigma\><rprime|'><around|(|l|)>=><verbatim|VObject>
  <math|x<rsub|class>> <em|pv> <math|\<varepsilon\><rsub|1>>>|<cell|>>|<row|<cell|>|<cell|<math|\<varepsilon\><rsub|g><rprime|'>>=<math|\<cdots\>>;<math|x<rsub|class>>:<math|l<rprime|'>>;<math|\<cdots\>>
  \ \ \ \ <math|\<sigma\><rprime|'><around|(|l<rprime|'>|)>=v>>|<cell|>>|<row|<cell|>|<cell|<math|<dfrac|<math-tt|
  \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ >|>>>|<cell|>>|<row|<cell|>|<cell|<verbatim|Prim
  "class" ><math|e,\<varepsilon\>,\<varepsilon\><rsub|g>,\<sigma\>>
  <math|\<Rightarrow\>> <math|v,\<varepsilon\><rprime|'><rsub|g>,\<sigma\><rprime|''>>>|<cell|>>>>>

  <section|The Surface Language and Desugaring>

  <subsection|Surface Language Syntax>

  STExp (<strong|S>mall<strong|T>alk <strong|Exp>ressions), the surface
  language used in this project, uses s-expression as its syntax
  representation. Using s-expression relieves the difficulty of parsing the
  <name|SmallTalk> programs. The workflow of interpreting/debugging a program
  follows:

  <\enumerate-alpha>
    <item>Use an existing SmallTalk implementation, Pharo, to generate the
    STExp expression from the SmallTalk source code.

    <item>Parse the s-expression, generate <name|Ocaml> STExp data structure.

    <item>Desugar the STExp to core language (CExp)

    <item>Run the CExp program to get the value.

    <item>If anything goes wrong, we can manually add debug code (such as
    print) to the STExp code and run the program again.
  </enumerate-alpha>

  STExp also serves as the language for built-in libraries. Figure 3 shows
  its data representation:

  <big-figure|<tabular|<tformat|<table|<row|<cell|<em|e>>|<cell|<math|\<in\>>>|<cell|STExp>|<cell|:=>|<cell|<verbatim|STId>
  <em|x>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STNum>
  <em|n>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STString>
  <em|x>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STNil>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STSelf>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STSuper>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STTrue>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STFalse>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STSendUnary>
  <math|e<rsub|receiver>> <math|x>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STSendBinary>
  <math|e<rsub|receiver>> <em|x> <em|e>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STSendKeyword>
  <math|e<rsub|receiver>> <em|x> <math|e<rsup|\<ast\>>>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STReturn>
  <em|e>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STSequence>
  <math|x<rsup|\<ast\>>> <math|e<rsup|\<ast\>>>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STAssign>
  <em|x> <em|e>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STLam>
  <math|x<rsup|\<ast\>>> <em|e>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STApp>
  <math|e> <math|e<rsup|\<ast\>>>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STPrim>
  <math|x> <math|e<rsup|\<ast\>>>>>|<row|<cell|>|<cell|>|<cell|>|<cell|\|>|<cell|<verbatim|STMethod>
  <math|x> <math|x> <math|x<rsup|\<ast\>>>
  <em|e>>>|<row|<cell|<math|x>>|<cell|<math|\<in\>>>|<cell|StrLit>|<cell|:=>|<cell|String
  Literal>>|<row|<cell|<math|n>>|<cell|<math|\<in\>>>|<cell|Number>|<cell|:=>|<cell|Number
  Literal>>>>>|The data representation of STExp>

  <subsection|<st> and STExp>

  Figure 4 presents the translation from <st> to STExp.

  <big-figure|<tabular|<tformat|<cwith|34|34|2|2|cell-hyphen|n>|<cwith|7|7|2|2|cell-hyphen|n>|<table|<row|<cell|<strong|<st>>>|<cell|<strong|STExp>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|self>>|<cell|<verbatim|STSelf>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|super>>|<cell|<verbatim|STSuper>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|1>>|<cell|<verbatim|(STNum
  1)>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|'a
  string'>>|<cell|<verbatim|(STSTring "a string")>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|anIdentifier>>|<cell|<verbatim|(STId
  anIdentifier)>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|nil>>|<cell|<verbatim|(STNil)>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|\|x
  y\|>>|<cell|<verbatim|(STSequence (x y)
  ())>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|\|x y\| x. y.
  nil.>>|<cell|<verbatim|(STSequence (x y)>>>|<row|<cell|>|<cell|
  \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ <verbatim|((STId x) (STId x)
  (STNil)))>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|[]>>|<cell|<verbatim|(STLam
  () (STSequence () ()))>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|[:arg1
  :arg2\| \ ]>>|<cell|<verbatim|(STLam (arg1 arg2) (STSequence ()
  ()))>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|[:arg1 :arg2\| \|x
  y\|]>>|<cell|<verbatim|(STLam (arg1 arg2) (STSequence (x y)
  ()))>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|[1]>>|<cell|<verbatim|(STLam
  () (STSequence () ((STNum 1))))>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|[^
  super]>>|<cell|<verbatim|(STLam () >>>|<row|<cell|>|<cell|<verbatim|
  \ \ \ \ \ \ (STSequence () ((STReturn STSuper))))>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|[:arg1
  :arg2\| \|x y\| 1]>>|<cell|<verbatim|(STLam (arg1
  arg2)>>>|<row|<cell|>|<cell|<verbatim| \ \ \ \ \ \ (STSequence (x y)
  ((STNum 1))))>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|Object
  asString>>|<cell|<verbatim|(STSendUnary (STId Object) asString>
  >>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|1 ==
  1>>|<cell|<verbatim|(STSendBinary (STNum 1) == (STNum
  1))>>>|<row|<cell|>|<cell|>>|<row|<cell|<verbatim|anId ifTrue:[]
  ifFalse:[]>>|<cell|<verbatim|(STSendKeyword (STId anId)
  ifTrue:ifFalse:>>>|<row|<cell|>|<cell|<verbatim|
  \ \ \ \ \ \ \ \ \ \ \ \ \ \ ((STLam () ...)>>>|<row|<cell|>|<cell|<verbatim|
  \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (STLam () ...)))>>>>>>|Examples of
  translating <st> to STExp>

  The <verbatim|STMethod> and <verbatim|STPrim> have no corresponding <st>
  structures. They are mainly for writing built-in libraries.

  <subsection|Elaboration>

  Before desugaring starts, there are a few places that require elaboration:

  <\enumerate>
    <item><verbatim|STSequence ><math|x<rsup|\<ast\>>>
    <verbatim|[]><math|\<rightsquigarrow\>><verbatim|STSequence
    ><math|x<rsup|\<ast\>>> <verbatim|[STNil]>

    In <st>, the body of a method, the body of a block, and the top-level
    programs are all represented as <verbatim|STSequence>. Two observations
    result in this elaboration:

    <\enumerate-alpha>
      <item>The value of applying a block is the value of the last expression
      in the body;

      <item>Empty block returns <verbatim|nil>.
    </enumerate-alpha>

    However, an empty method, i.e. an empty block being the method body,
    returns <verbatim|self>, not <verbatim|nil>. This is one of the reason
    for elaborating <verbatim|STMethod> (see 3).

    <item>Hoist undeclared variables in <verbatim|STSequence>. The left-hand
    side of <verbatim|STAssign> must be a declared variable. If the names are
    not declared in <verbatim|STSequence>, they should be added automatically
    to the list of temporaries. However, this hoist should only work in
    top-level programs. Any undeclared variable in a method might be an
    instance variable, in which case it should not be hoisted.

    <item><verbatim|STMethod> <math|x<rsub|class>> <math|x<rsub|method>>
    <math|x<rsup|\<ast\>><rsub|params>> <math|e<rsub|body>>, in which
    <math|e<rsub|body>> is a <verbatim|STSequence>

    <\description>
      <item*|self keyword>Method is desugared to <verbatim|Lam>. To bind the
      identifier <verbatim|*self*> to the current receiver in the
      <verbatim|Lam> body, <math|x<rsub|params><rsup|\<ast\>>> is going to be
      rewritten to <verbatim|*self*>,<math|x<rsub|1>>,<math|x<rsub|2>>,<math|\<cdots\>>,<math|x<rsub|n>>.
      For any method that does not return anything, <verbatim|self> is
      returned. Thus <verbatim|(STReturn STSelf)> is also appended to
      <math|e<rsub|body>> as the last expression.

      <item*|super keyword>The <verbatim|super> in <st> is actually an
      identifier refers to <verbatim|self>. That is, programmers can assign
      to <verbatim|super><\footnote>
        Yes, in <st>, programmers can freely declare <verbatim|super> as a
        temporary variable, or assign anything to <verbatim|super> in a
        method. Some implementation may report such an expression as an error
        in the development environment. You can try to use
        <verbatim|compile:> to circumvent this restriction. This is not true
        for <verbatim|self>.
      </footnote>, can pass the super as an argument, or even return
      <verbatim|super>. However, when <verbatim|super> is used as a receiver,
      <verbatim|super> always refers to the super class. This behavior
      clearly indicates that we need two ``<verbatim|super>''s in our model.
      <verbatim|STMethod> needs declare <verbatim|*super*> and
      <verbatim|super> as temporary variables, and then add the following
      assignments at the entry:

      <\render-code>
        <verbatim|STAssign "*super*" (STPrim "superclass" [STId>
        <math|x<rsub|class>><verbatim|])>

        <verbatim|STAssign "super" STSelf>\ 
      </render-code>

      In the body, if any <verbatim|STSend*> uses <verbatim|STSuper> as the
      receiver, this <verbatim|STSuper> refers to <verbatim|*super*>. Other
      <verbatim|STSuper> are desugared to <verbatim|(Id super)>, which is the
      same as <verbatim|self>.
    </description>
  </enumerate>

  <subsection|Desugaring>

  Desugaring function <math|\<cal-D\>\<cal-S\>> is defined as follows.

  <\render-code>
    <math|\<cal-D\>\<cal-S\>:>STExp<math|\<rightarrow\>>CExp

    \;

    <ds|<verbatim|STId> <math|x>> = <math|><verbatim|Id ><math|x>

    <ds|<verbatim|STNum> <math|n>> = <verbatim|Num> <math|n>

    <ds|<verbatim|STString> <math|x>> = <verbatim|String> <math|x>

    <ds|<verbatim|STNil>> = <verbatim|Id> <verbatim|"nil">

    <ds|<verbatim|STSelf>> = <verbatim|Id "*self*">

    <ds|<verbatim|STSuper>> = <verbatim|Id "super">

    <ds|<verbatim|STTrue>> = <verbatim|Id "true">

    <ds|<verbatim|STFalse>> = <verbatim|Id "false">

    \;

    <ds|<verbatim|STSendUnary> <em|e> <math|x>> = <em|desugar-send>
    \ <math|e> \ <math|x> <verbatim|[]>

    <ds|<verbatim|STSendBinary> <em|e> <em|x> <math|e<rsub|1>>> =
    <em|desugar-send> \ <em|e> <em|x> <verbatim|[><math|e<rsub|1>><verbatim|]>

    <ds|<verbatim|STSendKeyword> <em|e> <em|x> <math|e<rsup|+>>> =
    <em|desugar-send> \ <em|e> <em|x> <math|e<rsup|+>>

    \;

    <block|<tformat|<table|<row|<cell|Elaboration required>>>>>

    <ds|<verbatim|STSequence> <math|x<rsub|1>.x<rsup|\<ast\>>> <em|e>>=
    <verbatim|Let> <em|x> <verbatim|(Id "nil")> <ds|<verbatim|STSequence>
    <math|x<rsup|\<ast\>>> <em|e>>

    <ds|<verbatim|STSequence> <verbatim|[]>
    <math|e<rsub|1>.e<rsub|2>.e<rsup|\<ast\>>>> = <verbatim|Seq>
    <ds|<math|e<rsub|1>>> <ds|<verbatim|STSequence []
    ><math|e<rsub|2>.e<rsup|\<ast\>>>>

    <ds|<verbatim|STSequence> <verbatim|[]>
    <verbatim|[><math|e<rsub|1>><verbatim|]>> = <ds|<math|e<rsub|1>>>

    \;

    <ds|<verbatim|STAssign> <em|x> <math|e>> = <verbatim|Assign> <math|x>
    <ds|<em|e>>

    <ds|<verbatim|STReturn> <em|e>> = <verbatim|Return> <em|current-label>
    <ds|<em|e>>

    <ds|<verbatim|STPrim> <em|x> <math|e<rsup|\<ast\>>>> = <verbatim|Prim>
    <em|x> <ds|<math|e>><math|<rsup|\<ast\>>>

    \;

    <block|<tformat|<table|<row|<cell|Elaboration required>>>>>

    <ds|<verbatim|STMethod> <math|x<rsub|class>> <math|x<rsub|method>>
    <math|x<rsup|\<ast\>><rsub|params>> <math|e<rsub|body>>> =

    \ \ \ <verbatim|Prim "new-method" [String ><math|x<rsub|class>>;
    <verbatim|String ><math|x<rsub|method>>; <em|lam><verbatim|]>

    where\ 

    \ \ \ <math|e<rsub|body><rprime|'>> = <verbatim|WithLabel>
    <em|current-label> \ <ds|<math|e<rsub|body>>>

    \ \ \ <em|lam> = <verbatim|Lam *self*.><math|x<rsub|params><rsup|\<ast\>>>
    <math|e<rsub|body><rprime|'>>

    \ \ \ <em|current-label> is a fresh label.
  </render-code>

  <em|desugar-send> is an auxiliary function, which is defined as

  <\render-code>
    <em|desugar-send> <math|e<rsub|receiver>> <math|x<rsub|method>>
    <math|e<rsup|\<ast\>>> =

    <strong|match> <math|e<rsub|receiver>> <strong|with>

    \ \ \ <math|\<vartriangleright\>> <verbatim|STSuper>
    <math|\<rightarrow\>> <verbatim|Lookup> <math|x<rsub|method>>
    <verbatim|(Id "*super*")> <verbatim|(Id "self")>
    <ds|<em|e>><math|<rsup|\<ast\>>>

    \ \ \ <math|\<vartriangleright\>> <strong|else>\ 

    \ \ \ \ \ \ \ <verbatim|Let "recv"> <ds|<math|e<rsub|receiver>>>

    \ \ \ \ \ \ \ \ \ \ <verbatim|Let "class"> <verbatim|(Prim "class" [Id
    "recv"])>

    \ \ \ \ \ \ \ \ \ \ \ \ \ <verbatim|Lookup ><math|x<rsub|method>>
    <verbatim|(Id "class") (Id "recv")> <ds|<math|e>><math|<rsup|\<ast\>>> \ 
  </render-code>

  <section|Initial Environment and Store>

  Before starting the interpretation, the initial environment and store must
  contain some pre-existing classes for creating basic objects such as
  <verbatim|String>. Since creating instances (<verbatim|VObject>) requires
  to traverse the class hierarchy to construct environment for instance
  variables, the class hierarchy must also be ready for queries before the
  interpretation starts. The initial environment and store is constructed in
  file <verbatim|builtins.ml>.
</body>

<\initial>
  <\collection>
    <associate|info-flag|minimal>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-10|<tuple|4|6>>
    <associate|auto-11|<tuple|3.3|6>>
    <associate|auto-12|<tuple|3.4|7>>
    <associate|auto-13|<tuple|4|8>>
    <associate|auto-14|<tuple|5|?>>
    <associate|auto-15|<tuple|4|?>>
    <associate|auto-16|<tuple|4|?>>
    <associate|auto-17|<tuple|4|?>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|2|1>>
    <associate|auto-4|<tuple|2|1>>
    <associate|auto-5|<tuple|2|4>>
    <associate|auto-6|<tuple|3|5>>
    <associate|auto-7|<tuple|3.1|5>>
    <associate|auto-8|<tuple|3|5>>
    <associate|auto-9|<tuple|3.2|6>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|7>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|7>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|Expressions of the core language|<pageref|auto-2>>

      <tuple|normal|Values|<pageref|auto-3>>

      <tuple|normal|The data representation of STExp|<pageref|auto-8>>

      <tuple|normal|Examples of translating
      <with|font-shape|<quote|small-caps>|SmallTalk> to
      STExp|<pageref|auto-10>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Syntax
      and Value Domain of the Core Language>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Evaluation
      Rules> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4><vspace|0.5fn>

      <with|par-left|<quote|4tab>|Comments
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5><vspace|0.15fn>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>The
      Surface Language and Desugaring> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6><vspace|0.5fn>

      <with|par-left|<quote|1tab>|3.1<space|2spc>Surface Language Syntax
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|1tab>|3.2<space|2spc><with|font-shape|<quote|small-caps>|SmallTalk>
      and STExp <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|1tab>|3.3<space|2spc>Elaboration
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|1tab>|3.4<space|2spc>Desugaring
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>Initial
      Environment and Store> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>