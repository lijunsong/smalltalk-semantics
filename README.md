Dependencies
============
Ocaml >= 4.02 (4.01 might also work)
Pharo >= 3.0

No other Ocaml package is required.

Project Structures
==================
- bin/: Smalltalk reference implementation executables.
- kernel/: Smalltalk built-in classes.
- sexp/: A Ocaml library for parsing s-expression.
- test/: Test cases of the project.
- doc/: Documentation.
- main.d.byte: This binary file is generated as the main executable.

Download Pharo
==============
Run the following command:

 $ cd ./bin
 $ ./get-smalltalk.sh

After downloading the pharo, to check whether Pharo is working, execute

 $ ./pharo-ui Pharo.image

If you see the Pharo environment is opening, it works.


File In SExpExport.st
=====================
There is a smalltalk file SExpExport.st in ./bin directory. The file
contains the code that can generate STExp from SmallTalk source code.

To file in the SExpExport class, do the following in the Pharo environment:
 1. Right-click on the Pharo background to open the "World" menu
 2. Tools -> File Browser
 3. Find the ./bin directory in the project folder (the directory
    shall be the default one when file browser opens)
 4. Select SExpExport.st and click "Install"
 5. Open the "World" menu, choose "Save."
 6. Close Pharo.

To make sure the SExpExport.st is in the image, run the following command
in ./bin directory to make sure it works.

 $ ./pharo Pharo.image eval "SExpExport parseExpression: '1' "

The result is

 '(STNum 1) '

Compile The Ocaml Executable
=============================
Just run make in the project directory:

 $ make

To make sure everything works fine, run the test cases

 $ make test

Run the SmallTalk REPL
======================
To start the SmallTalk repl, use the option -repl:

 $ ./main.d.byte -repl

Now type in SmallTalk programs, and use Ctrl-d to starts the evaluation.
Use Ctrl-c to exit. The following shows one example of the interaction.

    $ ./main.d.byte -repl
    ====== Ctrl-D starts the evaluation ======
    '12' + '12' + 34
    ^D
    58
    ====== Ctrl-D starts the evaluation ======
    ^ 1
    Error: Return out of context
    ====== Ctrl-D starts the evaluation ======
    ^C
    $

Use -help to list all the available options.

Command line examples
=====================
The command line options work like a pipe, the output of previous option serves
as the input of the next option. For example

 - To load a SmallTalk file, parse it to STExp and print the STExp, do
   $ ./main.d.byte -load-st test/SelfSuperTest.st -stexp -print

 - To load SmallTalk source from stdin, parse it to STExp and print the STExp, do
   $ ./main.d.byte -stdin -stexp -print

 - To load SmallTalk source from stdin, observe the desugaring output, do
   $ ./main.d.byte -stdin -desugar -print

 - To load SmallTalk source from stdin, see the final result, do
   $ ./main.d.byte -stdin -desugar -answer -print

   OR just use -answer on the loaded source.
   $ ./main.d.byte -stdin -answer -print
