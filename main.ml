open Eval
open Interp
open Desugar
open Parse
open St_syntax
open Core_syntax
open Core_value
open Core_util
open Printf

type cell =
  | STExp of stexp
  | CExp of cexp
  | Result of result
  | Value of value * store
  | SmalltalkSource of string
  | STExpSource of string

let cell : cell ref = ref (SmalltalkSource "")

let type_of = function
  | STExp _ -> "STExp"
  | CExp _ -> "CExp"
  | Result _ -> "Result"
  | SmalltalkSource _ -> "Smalltalk Source"
  | STExpSource _ -> "STExp Source"
  | Value _ -> "Value"

let expect_err (expect : string) =
  failwith (sprintf "Command Line: expect %s, but got %s"
              expect (type_of !cell))

let cmd_load_smalltalk filepath =
  let contents = string_of_file filepath in
  cell := SmalltalkSource contents

let cmd_load_smalltalk_stdin () =
  let contents = read_all_stdin () in
  cell := SmalltalkSource contents

let cmd_load_stexp filepath =
  let contents = string_of_file filepath in
  cell := STExpSource contents

let cmd_print () = match !cell with
  | STExp exp -> print_endline (string_of_stexp exp)
  | CExp exp -> print_endline (string_of_cexp exp)
  | SmalltalkSource str -> print_endline str
  | STExpSource str -> print_endline str
  | Result (v, env, store) ->
    print_endline (string_of_env env);
    print_endline (string_of_store store);
    print_endline (string_of_value v);
  | Value (v, store) -> print_endline (pretty_of_value (real_object v store))

let cmd_desugar () : unit =
  let next : cexp = match !cell with
    | STExp exp -> desugar exp
    | SmalltalkSource str ->
      desugar (parse_smalltalk_string str)
    | STExpSource str ->  desugar (parse_stexp_source str)
    | _ -> expect_err "desugarable type"
  in
  cell := CExp next

let cmd_debug () : unit =
  let interp = interp_catch_error in
  let next : result = match !cell with
    | STExp exp -> interp (desugar exp)
    | CExp exp -> interp exp
    | Result r -> r
    | SmalltalkSource str ->
      str
      |> parse_smalltalk_string
      |> desugar
      |> interp
    | STExpSource str ->
      interp (desugar (parse_stexp_source str))
    | Value _ -> expect_err "non-value"
  in
  cell := Result next

let cmd_answer () : unit =
  try
    cmd_debug ();
    match !cell with
    | Result (v, _, store) -> cell := Value (v, store)
    | _ -> failwith "unreachable"
  with _ -> exit 0

let cmd_stexp () : unit =
  match !cell with
    | STExp exp -> ()
    | SmalltalkSource str ->
      cell := STExp (parse_smalltalk_string str)
    | STExpSource str ->
      cell := STExp (parse_stexp_source str)
    | _ -> expect_err "STExp or Smalltalk"

let cmd_smalltalk_repl () : unit =
  let rec repl genv store =
    try
      let answer_string, gen, sto = 
        printf "====== Ctrl-D starts the evaluation ======\n%!";
        let contents = read_all_stdin() in
        let exp = desugar (parse_smalltalk_string contents) in
        let v, genv', store' = eval_catch_error exp mt_env genv store in
        (pretty_of_value (real_object v store')), genv', store'
      in
      print_endline (Scanf.unescaped answer_string);
      repl gen sto
    with _ -> repl genv store
  in
  let g, s = Builtins.bootstrap () in
  repl g s
  
      
let cmd_verbose () : unit =
  verbose := true
  
let main =
  begin
    let speclist = [
      ("-verbose", Arg.Unit (cmd_verbose), "[debugging]Print verbose debugging information");
      ("-repl", Arg.Unit (cmd_smalltalk_repl), "Start smalltalk REPL.");
      ("-load-st", Arg.String (cmd_load_smalltalk), "Load smalltalk source code for processing");
      ("-load-stexp", Arg.String (cmd_load_stexp), "[debugging]Load STExp code for processing");
      ("-stdin", Arg.Unit (cmd_load_smalltalk_stdin), "Read smalltalk source code from stdin");
      ("-desugar", Arg.Unit (cmd_desugar), "Desugar the loaded code to exp");
      ("-stexp", Arg.Unit (cmd_stexp), "[debugging] Generate STExp from the given smalltalk source");
      ("-debug-interp", Arg.Unit (cmd_debug), "[debugging] evaluate the code to value, env, and store");      
      ("-answer", Arg.Unit (cmd_answer), "Evalaute loaded code to the final value");
      ("-print", Arg.Unit (cmd_print), "Print whatever current is (in cell)");
    ] in
    let usage_msg = "Just start using -repl. Other options are (order matters): " in
    Arg.parse speclist print_endline usage_msg
  end

let _ = main
