open St_syntax
open Printf
open Sexp_main
open Core_util

(* run the command and get its output *)
let cmd_output (cmd : string) : string =
  (*printf "cmd: %s\n%!" cmd;*)
  let in_channel = Unix.open_process_in cmd in
  read_all in_channel []


let parse_stexp_source (stexp : string) : stexp =
  (* clean the initial source, which may contain single quote *)
  let stexp' = (Str.global_replace (Str.regexp "^[ ']+\\|[ ']+$") "" stexp) in
  if (String.length stexp) = 0 then
    failwith "source is empty"
  else
    match parse_sexp stexp' with
    | [e] -> stexp_of_sexp e
    | lst -> sequentialize_stexp (List.map stexp_of_sexp lst)

let parse_smalltalk_string (st : string) : stexp =
  (* 1. run pharo to get STExp *)
  let cmd source =
    (* escape the single quote in the string *)
    let source' = (Str.global_replace (Str.regexp "'") "''" source) in
    (* escape the double quote *)
    let source'' = (Str.global_replace (Str.regexp "\"") "\\\"" source') in
    sprintf "./bin/pharo ./bin/Pharo.image eval \"SExpExport parseExpression: '%s' \"" source''
  in
  let stexp = cmd_output (cmd st) in
  (* 3. produce STExp from the s-expression *)
  parse_stexp_source stexp 

             
let parse_smalltalk_method (st : string) : stexp =
  let cmd source =
    (* escape the single quote in the string *)
    let source' = (Str.global_replace (Str.regexp "'") "''" source) in
    sprintf "./bin/pharo ./bin/Pharo.image eval \"SExpExport parseMethod: '%s' \"" source'
  in
  let stexp = cmd_output (cmd st) in
  parse_stexp_source stexp
